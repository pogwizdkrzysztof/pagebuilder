import {ObjectUtil} from "./lib/utils/ObjectUtil";
import {ColumnData} from "./stage/modules/data/ColumnData";
import {ImageData} from "./stage/modules/data/ImageData";
import {MainData} from "./stage/modules/data/MainData";
import {SectionData} from "./stage/modules/data/SectionData";
import {TitleData} from "./stage/modules/data/TitleData";

let main = new MainData();

let image1 = new ImageData();
image1.config.src = "http://placehold.it/200x100?text=jeden";
let image2 = new ImageData();
image2.config.src = "http://placehold.it/200x100?text=dwa";
let image3 = new ImageData();
image3.config.src = "http://placehold.it/200x100?text=trzy";

let title1 = new TitleData();
title1.config.headingText = "title jeden";
title1.id = "title-jeden";

let title2 = new TitleData();
title2.config.headingText = "title dwa";
title2.id = "title-dwa";

let title3 = new TitleData();
title3.config.headingText = "title trzy";
title3.id = "title-trzy";

var section1 = new SectionData();
section1.id = "section-jeden";
var column1 = new ColumnData();
column1.id = "column-jeden";

section1.submodulesIds = [column1.id];

title1.parentId = title2.parentId = title3.parentId = column1.id;

column1.parentId = section1.id;
column1.submodulesIds = [title1.id, title2.id, title3.id];


section1.submodulesIds = [column1.id];
section1.parentId = main.id;
main.submodulesIds = [section1.id];

const stageModulesInitialState_ = {
	byId: {
		[main.id]: main,
		[section1.id]: section1,
		[column1.id]: column1,
		[title1.id]: title1,
		[title2.id]: title2,
		[title3.id]: title3,
	},
	allIds: [
		main.id,
		section1.id,
		column1.id,
		title1.id,
		title2.id,
		title3.id,
	]};


const test = {
	byId: {
		// [image1.id]: image1,
		// [image2.id]: image2,
		// [image3.id]: image3,
		[main.id]: main,
		[section1.id]: section1,
		[column1.id]: column1,
		[title1.id]: title1,
		[title2.id]: title2,
		[title3.id]: title3,
	},
	allIds: [
		// image1.id,
		// image2.id,
		// image3.id,
		main.id,
		section1.id,
		column1.id,
		title1.id,
		title2.id,
		title3.id
	]
}


// export {stageModulesInitialState}

const state_01 = (_main) => {
	let main = ObjectUtil.clone(_main);
	main.submodulesIds = [];
	return {
		byId: {[main.id]: main},
		allIds: [main.id],
	};
};

const state_02 = (_main, _section1) => {
	let main = ObjectUtil.clone(_main);
	let section1 = ObjectUtil.clone(_section1);
	main.submodulesIds = [section1.id];
	section1.parentId = main.id;
	section1.submodulesIds = [];
	return {
		byId: {[main.id]: main, [section1.id]: section1},
		allIds: [main.id, section1.id],
	};
};

const state_03 = (_main, _section1, _column1) => {
	let main = ObjectUtil.clone(_main);
	let section1 = ObjectUtil.clone(_section1);
	let column1 = ObjectUtil.clone(_column1);
	main.submodulesIds = [section1.id];
	section1.parentId = main.id;
	section1.submodulesIds = [column1.id];
	column1.parentId = section1.id;
	column1.submodulesIds = [];
	return {
		byId: {[main.id]: main, [section1.id]: section1, [column1.id]: column1},
		allIds: [main.id, section1.id, column1.id],
	};
};

const state_04 = (_main, _section1, _column1, _title1) => {
	let main = ObjectUtil.clone(_main);
	let section1 = ObjectUtil.clone(_section1);
	let column1 = ObjectUtil.clone(_column1);
	let title1 = ObjectUtil.clone(_title1);
	main.submodulesIds = [section1.id];
	section1.parentId = main.id;
	section1.submodulesIds = [column1.id];
	column1.parentId = section1.id;
	column1.submodulesIds = [title1.id];
	title1.parentId = column1.id;
	return {
		byId: {[main.id]: main, [section1.id]: section1, [column1.id]: column1, [title1.id]: title1},
		allIds: [main.id, section1.id, column1.id, title1.id],
	};
};

const stageModulesInitialState = {
	selectedModuleId: null,
	past: [
		state_01(main),
		state_02(main, section1),
	],
	present:
		state_03(main, section1, column1),

	future: [
		state_04(main, section1, column1, title1),
	],
	temporary: {
		byId: {},
		allIds: [],
	},
};

export {stageModulesInitialState};