import classNames from "classnames";
import * as React from "react";
import {BaseProps, BaseState} from "../../App";
import {sc} from "../../lib/StyledComponents";


interface Props extends BaseProps {
	icon:string
}

@sc`
	&.icon {
		display: flex;
		font-size: 20px;
		margin-left: 8px;
		margin-right: -8px;
	}
`
export class ModulesListIcon extends React.Component<Props, BaseState> {

	public render() {
		const {icon, className} = this.props;

		const clazzName = classNames(
			"ModulesListIcon",
			className,
			'icon',
			`icon-${icon}`,
		);

		return <span className={clazzName}/>;
	}

}