import * as React from "react";
import {ConnectDragPreview, ConnectDragSource, DragSource, DragSourceSpec} from "react-dnd";
import {getEmptyImage} from "react-dnd-html5-backend";
import * as ReactDOM from "react-dom";
import {DragAndDropActions} from "../../actions/DragAndDropActions";
import {OutlinesActions} from "../../actions/OutlinesActions";
import {StageActions} from "../../actions/StageActions";
import {ToolbarsActions} from "../../actions/ToolbarsActions";
import {BaseProps, BaseState} from "../../App";
import {DragAndDropItemTypes} from "../../dnd/const/DragAndDropItemTypes";
import {DragItem} from "../../stage/modules/DragItem";
import {IModule} from "../../stage/modules/Module";
import {Icon} from "../Icon";


const moduleSource:DragSourceSpec<Props, DragItem> = {
	beginDrag(props:Props) {

		StageActions.saveModules();
		DragAndDropActions.beginDrag();
		// ToolbarsActions.clear();
		OutlinesActions.clear();

		return {
			itemType: DragAndDropItemTypes.PLAIN_MODULE,
			id: null,
			moduleType: props.module.id,
			name: props.module.name,
			icon: props.module.icon,
		} as DragItem;
	},
	endDrag(props:Props, monitor, component) {
		DragAndDropActions.endDrag();
		ToolbarsActions.clear();
		DragAndDropActions.updateDropInfo();
	},
};

const sourceCollect = function (connect, monitor) {
	return {
		isDragging: monitor.isDragging(),
		connectDragSource: connect.dragSource(),
		connectDragPreview: connect.dragPreview(),
	};
};


interface Props extends BaseProps {
	module:IModule,
	isDragging?:boolean,
	connectDragSource?:ConnectDragSource,
	connectDragPreview?:ConnectDragPreview
}


@(DragSource(DragAndDropItemTypes.PLAIN_MODULE, moduleSource, sourceCollect) as any)
export class ModulesListItem extends React.Component<Props, BaseState> {

	private ref:Element = null;

	public componentDidMount() {
		// Use empty image as a drag preview so browsers don't draw it
		// and we can draw whatever we want on the custom drag layer instead.
		this.props.connectDragPreview &&
		this.props.connectDragPreview(getEmptyImage(), {
			// IE fallback: specify that we'd rather screenshot the node
			// when it already knows it's being dragged so we can hide it with CSS.
			captureDraggingState: true,
		});
	}

	private handleRef = (instance) => {
		const node = ReactDOM.findDOMNode(instance) as Element;
		this.ref = node;
		this.connectDnD(node);
	};

	private connectDnD = (node:Element) => {
		if (this.props.connectDragSource)
			this.props.connectDragSource(node, {dropEffect: 'copy'});
	};

	private handleClick = () => {
		StageActions.addModules(this.props.module);
	};

	public render() {

		const {module} = this.props;

		return <div className={"ModulesListItem"}
					ref={this.handleRef}
					onClick={this.handleClick}
		>
			<Icon icon={module.icon}/> <span>{module.name}</span>
		</div>;
	}

}