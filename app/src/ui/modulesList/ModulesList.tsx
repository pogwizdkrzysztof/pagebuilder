import * as React from "react";
import {ReactNode} from "react";
import {Scrollbars} from 'react-custom-scrollbars';
import {MapStateToProps} from "react-redux";
import {BaseProps, BaseState} from "../../App";
import {combine} from "../../lib/redux/ReduxDecorators";
import {RootState} from "../../reducers/RootReducer";
import {ModulesState} from "../../reducers/states/ModulesState";
import {ModulesSelectors} from "../../selectors/ModulesSelectors";
import {IModule} from "../../stage/modules/Module";
import {ModulesListItem} from "./ModulesListItem";

interface StateProps {
	modules:ModulesState
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {
	return {
		modules: ModulesSelectors.getModules(state),
	};
};


type ExternalProps = Partial<StateProps>;

interface Props extends BaseProps, ExternalProps {}

@combine(mapStateToProps)
export class ModulesList extends React.Component<Props, BaseState> {

	public render() {
		return (<Scrollbars>
			<div className={"ModulesList"}>
				{this.props.modules.map((module:IModule) => <ModulesListItem key={module.id} module={module}/>)}
			</div>
		</Scrollbars>) as ReactNode;
	}
}