import * as React from "react";
import {BaseProps, BaseState} from "../App";
import {Icon} from "./Icon";

interface Props extends BaseProps {
	tabName:string
}

export class TODOPanel extends React.Component<Props, BaseState> {

	render() {
		return <div className={"TODOPanel"}>
			<Icon icon={'hammer'}/>
			<div>TODO <strong>{this.props.tabName}</strong></div>
		</div>;
	}
}