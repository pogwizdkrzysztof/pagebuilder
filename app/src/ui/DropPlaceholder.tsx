import classNames from "classnames";
import * as React from "react";
import {BaseProps} from "../App";
import {sc} from "../lib/StyledComponents";

@sc`
	flex: 1 0 auto;
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 10px;
	background-color: rgba(0, 0, 0, 0.025);
	
	.icon {
		display: flex;
		align-items: center;
		justify-content: center;
		font-size: 30px;
		color: rgba(0,0,0,0.15);
	}
`
export class DropPlaceholder extends React.PureComponent<BaseProps> {

	public render() {
		const {className} = this.props;

		const clazzNames = classNames(
			"DropPlaceholder",
			className,
		);

		return (
			<div className={clazzNames}>
				<span className={classNames("icon", "icon-plus-circle")}></span>
			</div>
		);
	}

}