import * as React from "react";
import {BaseProps, BaseState} from "../App";
import {Icon} from "./Icon";

export class AppLoader extends React.Component<BaseProps, BaseState> {
	public render() {
		return (
			<div className="AppSplash AppLoader">
				<Icon icon={'logo'}/>
				<h1>PageBuildr <small>Loading...</small></h1>
			</div>
		);
	}
}