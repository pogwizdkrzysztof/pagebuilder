import classNames from "classnames";
import * as React from "react";
import {MapStateToProps} from "react-redux";
import {BaseProps, BaseState} from "../App";
import {DropAxis} from "../dnd/const/DropAxis";
import {DropSide} from "../dnd/const/DropSide";
import {DnDUtil} from "../dnd/utils/DnDUtil";
import {combine} from "../lib/redux/ReduxDecorators";
import {RootState} from "../reducers/RootReducer";
import {DnDSelectors} from "../selectors/DnDSelectors";

type ExternalProps = Partial<StateProps>;

interface Props extends BaseProps, ExternalProps {}

interface StateProps {
	dropRect:ClientRect,
	dropSide:DropSide,
	isDropAllowed:boolean,
	isInnerDrop:boolean
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {
	const dropSide = DnDSelectors.getDropSide(state);
	const rect = DnDSelectors.getDropOverModuleRect(state);
	const isDropAllowed = DnDSelectors.getDragModuleId(state) != DnDSelectors.getDropOverModuleId(state);
	const isInnerDrop = DnDSelectors.getDropModuleId(state) === DnDSelectors.getDropOverModuleId(state);
	return {
		dropRect: rect,
		dropSide: dropSide,
		isDropAllowed: isDropAllowed,
		isInnerDrop: isInnerDrop,
	};
};


@combine(mapStateToProps)
export class DnDPlaceholder extends React.PureComponent<Props, BaseState> {

	public render() {

		const {dropSide, dropRect, isDropAllowed, isInnerDrop} = this.props;

		if (!dropRect || !dropSide)
			return null;

		let dropAxis = DnDUtil.getDropAxis(dropSide);

		let className = classNames({
			"DragAndDropPlaceholder": true,
			"disallowed": !isDropAllowed,
			[dropSide.toString().toLowerCase()]: true,
			"horizontal": dropAxis === DropAxis.X,
			"vertical": dropAxis === DropAxis.Y,
			"inner": isInnerDrop,
			"outer": !isInnerDrop,
		});

		let iconClassName = classNames(
			"icon",
		);

		let style = {
			top: dropRect.top,
			left: dropRect.left,
			width: dropRect.width,
			height: dropRect.height,
		};

		return (
			<div className={className}
				 style={style}
			>
				<div className={"DnDLine"}></div>
				<div className={"DnDIcon"}>
					<span className={iconClassName}></span>
				</div>
			</div>
		);

	}

}