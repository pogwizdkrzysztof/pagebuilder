import * as React from "react";
import {BaseProps, BaseState} from "../App";

export class UI extends React.Component<BaseProps, BaseState> {

	render() {
		return (
			<div className={"UI"}>
				{this.props.children}
			</div>
		);
	}

}