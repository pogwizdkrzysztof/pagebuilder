import * as React from "react";
import {BaseProps} from "../App";

interface Props extends BaseProps {
	icon:string
}

export class Icon extends React.Component<Props> {

	render() {
		return (<span className={`Icon icon icon-${this.props.icon}`}/>);
	}

}