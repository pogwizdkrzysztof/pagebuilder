import * as React from "react";
import {BaseProps, BaseState} from "../App";
import {Icon} from "./Icon";

export class AppError extends React.Component<BaseProps, BaseState> {
	public render() {
		return (
			<div className="AppSplash AppError">
				<Icon icon={'logo'}/>
				<h1>PageBuildr <small>Error has occurred. Please try again later.</small></h1>
			</div>
		);
	}
}