import * as React from "react";
import {MapStateToProps} from "react-redux";
import {BaseProps, BaseState} from "../../App";
import {combine} from "../../lib/redux/ReduxDecorators";
import {RootState} from "../../reducers/RootReducer";
import {OutlinesSelectors} from "../../selectors/OutlinesSelectors";
import {StageSelectors} from "../../selectors/StageSelectors";
import {ModulesUtil} from "../../stage/modules/utils/ModulesUtil";


interface StateProps {
	allIds:string[],
	byId:{ [key:string]:ClientRect }
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {
	return {
		allIds: OutlinesSelectors.getAllIds(state),
		byId: OutlinesSelectors.getById(state),
	};
};

type ExternalProps = Partial<StateProps>;

interface Props extends BaseProps, ExternalProps {}

@combine(mapStateToProps)
export class Outlines extends React.PureComponent<Props, BaseState> {

	protected renderOutlines = () => {
		return this.props.allIds.map((id, index) => {
			let rect = this.props.byId[id];
			if (!rect) //have to wait for next rerender, because outline called externally. not directly from stage module
				return null;
			let style = {
				left: rect.left,
				top: rect.top,
				width: rect.width,
				height: rect.height,
			};
			let module = StageSelectors.getModuleData(id);
			if (!module) {
				module = StageSelectors.getTemporaryModuleData(id);
			}
			if (!module) {
				return null;
			}
			let moduleName = ModulesUtil.getPolicy(module).name;
			return <div key={index}
						className={"Outline"}
						style={style}
			>
				<span>{moduleName}</span>
			</div>;
		});
	};

	public render() {

		return (
			<div className="Outlines">
				{this.renderOutlines()}
			</div>
		);
	}

}