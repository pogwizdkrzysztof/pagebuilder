import classNames from "classnames";
import * as React from "react";
import {CSSProperties} from "react";
import {getEmptyImage} from "react-dnd-html5-backend";
import * as ReactDOM from "react-dom";
import {MapStateToProps} from "react-redux";
import {AppUIActions} from "../../actions/AppUIActions";
import {StageActions} from "../../actions/StageActions";
import {ToolbarsActions} from "../../actions/ToolbarsActions";
import {BaseProps, BaseState} from "../../App";
import {AppUITab} from "../../const/AppUITab";
import {MoveDirection} from "../../const/MoveDirection";
import {ModuleDragSource} from "../../dnd/DnDDecorators";
import {combine} from "../../lib/redux/ReduxDecorators";
import {M} from "../../models/M";
import {RootState} from "../../reducers/RootReducer";
import {DnDSelectors} from "../../selectors/DnDSelectors";
import {StageSelectors} from "../../selectors/StageSelectors";
import {ToolbarsSelectors} from "../../selectors/ToolbarsSelectors";
import {ModuleData} from "../../stage/modules/data/ModuleData";
import {ModulesUtil} from "../../stage/modules/utils/ModulesUtil";
import {ModuleDnDProps} from "../../stage/modules/view/ModuleView";
import {ToolbarData} from "../../stageUI/ToolbarData";


interface StateProps {
	isHidden:boolean,
	allIds:string[],
	byId:{ [key:string]:ToolbarData }
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {
	return {
		isHidden: DnDSelectors.isDragging(state),
		allIds: ToolbarsSelectors.getAllIds(state),
		byId: ToolbarsSelectors.getById(state),
	};
};

type ExternalProps = Partial<StateProps>;

interface Props extends BaseProps, ExternalProps {}

@combine(mapStateToProps)
export class Toolbars extends React.PureComponent<Props, BaseState> {

	protected renderToolbars = () => {
		return this.props.allIds.map((id, index) => {
			let toolbarData = this.props.byId[id];
			let moduleData = M.store.getState().stage.present.byId[id];
			return <Toolbar key={index}
							isHidden={this.props.isHidden}
							moduleData={moduleData}
							toolbarData={toolbarData}
			/>;
		});
	};

	public render() {
		return (
			<div className="Toolbars">
				{this.renderToolbars()}
			</div>
		);
	}
}


interface ToolbarProps {
	isHidden:boolean,
	moduleData:ModuleData,
	toolbarData:ToolbarData
}

class Toolbar extends React.PureComponent<ToolbarProps> {


	protected _getModulePath = (moduleData:ModuleData = this.props.moduleData, path:ModuleData[] = []) => {
		if (moduleData)
			path.push(moduleData);
		let parentModule = M.store.getState().stage.present.byId[moduleData.parentId];
		if (parentModule)
			this._getModulePath(parentModule, path);
		return path;
	};

	protected _getModulePathView = () => {
		let path = this._getModulePath();
		path.reverse();
		return path.map((moduleData) => {
			const modulePolicy = ModulesUtil.getPolicy(moduleData);
			if (moduleData.id === this.props.moduleData.id || !moduleData.parentId)
				return null;

			return (
				<div key={moduleData.id}
					 className={"module-path-item"}
				>
					<ToolbarButton moduleData={moduleData}
								   label={modulePolicy.name}
								   icon={modulePolicy.icon}
								   onClick={() => {
									   StageActions.selectModule(moduleData.id);
								   }}
					/>
					<div>
						<ToolbarConfigButton moduleData={moduleData}/>
						<ToolbarDuplicateButton moduleData={moduleData}
												isDisabled={!modulePolicy.isDuplicable}
						/>
						{/*<ToolbarMoveButton label={"Move Up"}
										   moduleData={moduleData}
										   direction={MoveDirection.UP}
										   icon={"move-up"}
						/>
						<ToolbarMoveButton label={"Move Down"}
										   moduleData={moduleData}
										   direction={MoveDirection.DOWN}
										   icon={"move-down"}
						/>*/}
						<ToolbarDragButton moduleData={moduleData}
										   style={this.props.isHidden ? {pointerEvents: "auto"} : {}}
						/>
						<ToolbarRemoveButton moduleData={moduleData}
											 isVisible={modulePolicy.isRemovable}
						/>
					</div>
				</div>
			);
		});
	};


	public render() {

		const moduleData = this.props.moduleData;
		const modulePolicy = ModulesUtil.getPolicy(moduleData);
		const rect = this.props.toolbarData.moduleRect;

		const style = {
			opacity: !this.props.isHidden ? 1 : 0,
			top: rect.top,
			left: rect.left,
			height: rect.height,
			width: rect.width,
		};

		return <div className={"Toolbar"} style={style}>

			<div className="ModuleToolbarBorder"/>
			<div className="ModuleToolbarName">
				{modulePolicy.name}
				<span className={"module-id"}>{moduleData.id}</span>
			</div>
			<div className="ModuleToolbarPanel" style={this.props.isHidden ? {pointerEvents: "none"} : {}}>
				<div className="module-path">
					{this._getModulePathView()}
				</div>
				<div className="module-actions">
					<ToolbarConfigButton moduleData={moduleData}/>
					<ToolbarDuplicateButton moduleData={moduleData}
											isDisabled={!modulePolicy.isDuplicable}
					/>
					{/*<ToolbarMoveButton label={"Move Up"}
									   moduleData={moduleData}
									   direction={MoveDirection.UP}
									   icon={"move-up"}
					/>
					<ToolbarMoveButton label={"Move Down"}
									   moduleData={moduleData}
									   direction={MoveDirection.DOWN}
									   icon={"move-down"}
					/>*/}
					<ToolbarDragButton moduleData={moduleData}
									   style={this.props.isHidden ? {pointerEvents: "auto"} : {}}
					/>
					<ToolbarRemoveButton moduleData={moduleData}
										 isVisible={modulePolicy.isRemovable}
					/>
				</div>
			</div>

		</div>;
	}

}

interface ToolbarButtonProps extends BaseProps {
	moduleData:ModuleData,
	label:string,
	icon:string,
	onClick:(event) => void,
	isDisabled?:boolean,
	isVisible?:boolean,
	innerRef?:(ref) => void,
	style?:CSSProperties
}

class ToolbarButton extends React.PureComponent<ToolbarButtonProps> {

	public static defaultProps = {
		moduleData: null,
		label: "",
		icon: "",
		onClick: () => null,
		innerRef: (ref) => null,
		isDisabled: false,
		isVisible: true,
		style: {},
	};

	public render() {
		let {label, icon, onClick, innerRef, isDisabled, isVisible, className, style} = this.props;
		return <button title={label}
					   className={classNames("ToolbarButton", className)}
					   onClick={onClick}
					   disabled={isDisabled}
					   ref={innerRef}
					   style={style}
		>
			<span className={classNames("icon", `icon-${icon}`)}/>
		</button>;
	}

}

const ToolbarRemoveButton = (props:Partial<ToolbarButtonProps>) => {
	return <ToolbarButton label={"Remove"}
						  icon={"bin"}
						  className={"remove"}
						  isVisible={props.isVisible}
						  onClick={(event) => {
							  event.stopPropagation();
							  ToolbarsActions.clear();
							  StageActions.removeModules([props.moduleData]);
						  }}
	/>;
};

const ToolbarMoveButton = (props:Partial<ToolbarButtonProps> & { direction:MoveDirection }) => {

	//@todo: move to util
	const canMove = (direction:MoveDirection):boolean => {
		let moduleId = props.moduleData.id;
		let parentId = props.moduleData.parentId;
		let parent = M.store.getState().stage.present.byId[parentId];

		if (!parent)
			return false;

		switch (direction) {
			case MoveDirection.UP:
				return parent.submodulesIds.indexOf(moduleId) > 0;
			case MoveDirection.DOWN:
				return parent.submodulesIds.indexOf(moduleId) < parent.submodulesIds.length - 1;
		}
	};


	return <ToolbarButton label={props.label}
						  icon={props.icon}
						  className={"move"}
						  isDisabled={!canMove(props.direction)}
						  isVisible={props.isVisible}
						  onClick={(event) => {
							  event.stopPropagation();
							  StageActions.moveModules([props.moduleData], props.direction);
						  }}
	/>;
};

const ToolbarDuplicateButton = (props:Partial<ToolbarButtonProps>) => {
	return <ToolbarButton label={"Duplicate"}
						  icon={"clone"}
						  className={"clone"}
						  isVisible={props.isVisible}
						  isDisabled={props.isDisabled}
						  onClick={(event) => {
							  event.stopPropagation();
							  StageActions.cloneModules([props.moduleData]);
							  let selectedModule = StageSelectors.getSelectedModuleData();
							  StageActions.forceUpdateModules([selectedModule]);
						  }}
	/>;
};

const ToolbarConfigButton = (props:Partial<ToolbarButtonProps>) => {

	const handleClick = (event:MouseEvent) => {
		event.stopPropagation();
		StageActions.toggleEditModule(props.moduleData.id);
		AppUIActions.toggleTab(AppUITab.UI_CONFIG_TAB);
	};

	return <ToolbarButton label={"Config"}
						  icon={"config"}
						  className={"settings"}
						  isVisible={props.isVisible}
						  isDisabled={props.isDisabled}
						  onClick={handleClick}
	/>;
};

interface MoveComponentButtonProps extends Partial<ToolbarButtonProps>, ModuleDnDProps {
	moduleData:ModuleData
}

@ModuleDragSource
class ToolbarDragButton extends React.PureComponent<MoveComponentButtonProps> {

	public ref = null;

	public componentDidMount() {
		this.props.connectDragPreview(getEmptyImage(), {
			captureDraggingState: true,
		});
	}

	private connectDnD = (node:Element) => {
		if (this.props.connectDragSource)
			this.props.connectDragSource(node);
	};

	public render() {

		let {moduleData} = this.props;

		return <ToolbarButton moduleData={moduleData}
							  icon={"move"}
							  label={"Drag"}
							  innerRef={instance => {
								  const node = ReactDOM.findDOMNode(instance) as Element;
								  this.ref = node;
								  this.connectDnD(node);
							  }}
							  style={this.props.style}
		/>;
	}
}