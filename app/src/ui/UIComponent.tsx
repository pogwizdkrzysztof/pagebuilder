import * as React from "react";

export interface UIProps {}

export interface UIState {}

export abstract class UIComponent<P extends UIProps = UIProps, S extends UIState = UIState>
	extends React.Component<P, S> {

}