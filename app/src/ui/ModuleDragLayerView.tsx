import * as React from "react";
import {DragLayer} from 'react-dnd';
import {BaseProps, BaseState} from "../App";
import {DragAndDropItemTypes} from "../dnd/const/DragAndDropItemTypes";

const layerStyles = {
	position: "fixed" as "fixed",
	pointerEvents: 'none' as 'none',
	zIndex: 100,
	left: 0,
	top: 0,
	width: '100%',
	height: '100%'
};


function getItemStyles(props) {
	const {currentOffset} = props;
	if (!currentOffset) {
		return {
			display: 'none'
		};
	}

	const {x, y} = currentOffset;
	const transform = `translate(${x}px, ${y}px)`;
	return {
		transform: transform,
		WebkitTransform: transform
	};
}

interface Props extends BaseProps {
	item?:Object,
	itemType?:DragAndDropItemTypes,
	currentOffset?:Object,
	isDragging?:boolean
}

@(DragLayer(monitor => ({
	item: monitor.getItem(),
	itemType: monitor.getItemType(),
	currentOffset: monitor.getClientOffset(),
	isDragging: monitor.isDragging()
})) as any)
export class ModuleDragLayerView extends React.Component<Props, BaseState> {

	renderItem(type, item) {
		switch (type) {
			case DragAndDropItemTypes.STAGE_MODULE:
			case DragAndDropItemTypes.PLAIN_MODULE:
				return (
					<div className={"ModuleDragLayer" + " " + item.moduleType}>
						<span className={"icon icon-" + item.icon}></span>
						<span className="title">{item.name}</span>
					</div>
				);
		}
	}

	render() {
		const {item, itemType, isDragging} = this.props;
		if (!isDragging)
			return null;

		return (
			<div style={layerStyles}>
				<div style={getItemStyles(this.props)}>
					{this.renderItem(itemType, item)}
				</div>
			</div>
		);
	}
}