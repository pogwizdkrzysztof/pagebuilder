import * as React from "react";
import {OutlinesActions} from "../../actions/OutlinesActions";
import {StageActions} from "../../actions/StageActions";
import {BaseProps, BaseState} from "../../App";
import {ModuleData} from "../../stage/modules/data/ModuleData";

interface Props extends BaseProps {
	mainModule:ModuleData,
	modules:{ [key:string]:ModuleData }
}


export class DebugModulesTree extends React.Component<Props, BaseState> {

	protected renderBranch(module:ModuleData, modules:{ [key:string]:ModuleData }, index:number = 0) {
		let children = module.submodulesIds.map((id, index) => {
			return this.renderBranch(modules[id], modules, index);
		});
		let subBranch = React.createElement("ul", {children});

		return <li key={index}
				   onClick={(event) => {
					   event.stopPropagation();
					   StageActions.selectModule(module.id);
				   }}
				   onMouseOver={(event) => {
					   event.stopPropagation();
					   OutlinesActions.clear();
					   OutlinesActions.add(module.id);
				   }}
				   onMouseLeave={(event) => {
					   event.stopPropagation();
					   OutlinesActions.clear();
				   }}
		>
			<span>{module.module} ({module.id.toString().slice(0, 8)})</span>
			{subBranch}
		</li>;
	}

	public render() {
		return <div className={"DebugModulesTree"}>
			<ul className={"debug-tree"}>
				{this.renderBranch(this.props.mainModule, this.props.modules)}
			</ul>
		</div>;
	}


}