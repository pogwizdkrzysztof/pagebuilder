import classNames from "classnames";
import * as React from "react";
import {Scrollbars} from 'react-custom-scrollbars';
import {MapStateToProps} from "react-redux";
import {DebugActions} from "../../actions/DebugActions";
import {BaseProps, BaseState} from "../../App";
import {combine} from "../../lib/redux/ReduxDecorators";
import {StorageUtil} from "../../lib/utils/StorageUtil";
import {M} from "../../models/M";
import {RootState} from "../../reducers/RootReducer";
import {StageSelectors} from "../../selectors/StageSelectors";
import {DebugModulesTree} from "./DebugModulesTree";


interface State extends BaseState {
	isVisible:boolean
}

interface StateProps {
	modulesCount:number;
	modules:any; //@todo type it
	mainModule:any; //@todo type it
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {
	const allModules = StageSelectors.getById(state);
	const allModulesIds = StageSelectors.getAllIds(state);
	const mainModuleData = StageSelectors.getMainModuleData(state);
	return {
		modulesCount: allModulesIds.length,
		modules: allModules,
		mainModule: mainModuleData,
	};
};

type ExternalProps = Partial<StateProps>;

interface Props extends BaseProps, ExternalProps {}

@combine(mapStateToProps)
export class DebugPanel extends React.Component<Props, State> {

	constructor(props) {
		super(props);

		this.state = {
			isVisible: false,
		};
	}

	public componentDidMount():void {
		document.addEventListener('keydown', this.togglePanel);
	}

	public componentWillUnmount():void {
		document.removeEventListener('keydown', this.togglePanel);
	}

	private togglePanel = (event) => {
		event = event || window.event;
		if (event.keyCode == '120') {
			this.setState({isVisible: !this.state.isVisible});
		}
	};

	protected saveState() {
		StorageUtil.saveState(M.store.getState());
	}

	public render() {

		let className = classNames(
			"DebugPanel",
		);

		const {modules, mainModule, modulesCount} = this.props;

		return <Scrollbars>
			<div className={className}>
				{/*<fieldset>
					<legend>Modules actions:</legend>
					<button onClick={DebugActions.toggleIds}>toggle ids</button>
					<button onClick={DebugActions.toggleBorders}>toggle borders</button>
				</fieldset>*/}

				<fieldset>
					<legend>Model:</legend>
					<button onClick={this.saveState}>save</button>
					<button onClick={DebugActions.loadState}>load</button>
				</fieldset>

				<fieldset>
					<legend>Modules ({modulesCount}):</legend>
					<DebugModulesTree modules={modules}
									  mainModule={mainModule}
					/>
				</fieldset>
			</div>
		</Scrollbars>;
	}


}