import * as React from "react";
import {ComponentClass} from "react";
import {Scrollbars} from 'react-custom-scrollbars';
import {MapStateToProps} from "react-redux";
import {AppUIActions} from "../../actions/AppUIActions";
import {StageActions} from "../../actions/StageActions";
import {BaseProps, BaseState} from "../../App";
import {AppUITab} from "../../const/AppUITab";
import {combine} from "../../lib/redux/ReduxDecorators";
import {ObjectUtil} from "../../lib/utils/ObjectUtil";
import {ModulesModel} from "../../models/ModulesModel";
import {RootState} from "../../reducers/RootReducer";
import {ModuleConfigProps} from "../../stage/modules/configView/ModuleConfigView";
import {ModuleData} from "../../stage/modules/data/ModuleData";
import {ModuleType} from "../../stage/modules/ModuleType";
import {ModulesUtil} from "../../stage/modules/utils/ModulesUtil";
import {Icon} from "../Icon";

interface StateProps {
	moduleData:ModuleData;
	initialModuleData:ModuleData;
	moduleConfigView:any;
	selectedModuleId:string
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {

	const basicModuleId = state.stage.selectedModuleId;
	const configModules = state.stage.temporary;
	const basicModuleData = basicModuleId && configModules.byId[basicModuleId];
	const moduleConfigView = basicModuleData ? ModulesModel.configs[basicModuleData.module] : null;
	const moduleData = basicModuleData ? basicModuleData : null;
	const initialModuleData = basicModuleData ? state.stage.present.byId[configModules.allIds[0]] : null;

	return {
		moduleData: moduleData,
		initialModuleData: initialModuleData,
		moduleConfigView: moduleConfigView,
		selectedModuleId: state.stage.selectedModuleId,
	};
};

type ExternalProps = Partial<StateProps>;

interface Props extends BaseProps, ExternalProps {}

@combine(mapStateToProps)
export class ModuleConfigurator extends React.Component<Props, BaseState> {

	public componentWillUnmount():void {
		this._onCancel();
	}

	protected _onCancel = () => {
		StageActions.restoreModules([this.props.initialModuleData]);
	};

	protected _onSave = () => {
		StageActions.saveModules();
		AppUIActions.toggleTab(AppUITab.UI_CONFIG_TAB);
	};


	protected _getElements() {
		if (this.props.moduleData) {
			return React.createElement<ModuleConfigProps>(
				this.props.moduleConfigView as ComponentClass<ModuleConfigProps>,
				{
					moduleData: this.props.moduleData as ModuleData,
					onChange: (moduleData:ModuleData) => {
						StageActions.updateModules([moduleData]);
					},
					addChild: (moduleType:ModuleType) => {
						//@todo: large logic. maybe move to one other place?
						let module = ModulesModel.getModule(moduleType);
						let modulesDatas = module.getModuleData();
						let parentModuleData = ObjectUtil.deepClone(this.props.moduleData);
						//@todo: what if "module.getModuleData()" return more than 1 child? FIX THIS!
						ModulesUtil.setParent(modulesDatas[0], parentModuleData);
						modulesDatas.unshift(parentModuleData);
						StageActions.addChildModule(modulesDatas);
					},
					removeChild: () => {
						//@todo: large logic. maybe move to one other place?
						let parentModuleData = ObjectUtil.deepClone(this.props.moduleData);
						//@todo: what if "module.getModuleData()" return more than 1 child? FIX THIS!
						let submoduleId = parentModuleData.submodulesIds[parentModuleData.submodulesIds.length - 1];
						if (submoduleId) {
							ModulesUtil.removeSubmodule(parentModuleData,
								parentModuleData.submodulesIds[parentModuleData.submodulesIds.length - 1]);
							StageActions.removeChildModule([parentModuleData], submoduleId);
						}
					},
				});
		}
		return null;
	}

	render() {

		if (!this.props.moduleData) {
			return <div key={"empty"} className={"ModuleConfigurator empty"}>
				<Icon icon={'info'}/>
				<div>Select stage module</div>
			</div>;
		}

		return (
			<div key={"form"} className={"ModuleConfigurator"}>
				<Scrollbars className={"configurator-content"}>
					{this._getElements()}
				</Scrollbars>

				<div className={"configurator-actions"}>
					<button onClick={this._onCancel}>
						Cancel
					</button>
					<button onClick={this._onSave}>
						Save
					</button>
				</div>
			</div>
		);
	}
}