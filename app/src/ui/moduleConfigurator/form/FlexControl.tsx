import classNames from "classnames";
import {
	AlignContentProperty,
	AlignItemsProperty,
	DisplayProperty,
	FlexDirectionProperty,
	FlexWrapProperty,
	JustifyContentProperty,
} from "csstype";
import * as React from "react";
import {CSSProperties} from "react";
import {BaseProps} from "../../../App";
import {sc} from "../../../lib/StyledComponents";


interface Props extends BaseProps {
	help:string,
	value:string,
	childQuantity:number,
	property?:string,
	flexbox?:CSSProperties,
	onClick?:(property:string, value:string | number) => void
}


const getStyle = <T1 extends {}>(field) => ((props:Props):T1 => {
	if (field === props.property) {
		return props.value as unknown as T1;
	}
	return props.flexbox[field] as unknown as T1;
}) as unknown as T1;

@sc`
	flex-direction: ${getStyle<FlexDirectionProperty>('flexDirection')};
	display: ${getStyle<DisplayProperty>('display')};
	flex-wrap: ${getStyle<FlexWrapProperty>('flexWrap')};
	justify-content: ${getStyle<JustifyContentProperty>('justifyContent')};
	align-items: ${getStyle<AlignItemsProperty>('alignItems')};
	align-content: ${getStyle<AlignContentProperty>('alignContent')};

	opacity: 0.3;

	width: 75px;
	height: 75px;
	border: 1px solid #555;
	user-select: none;
	cursor: pointer;

	box-sizing: border-box;
	border-radius: 2px;
	border-color: #c3c3c3;
	box-shadow: 0px 1px 2px 1px #eee;
	
	&.boxSelected {
		opacity: 1;
	}
	
	.item {
		box-sizing: border-box;
		width: 20px;
		height: 20px;
		border: 1px solid #aaa;
		margin: 1px;
		border-radius: 2px;
		text-align: center;
		display: flex;
		align-items: center;
		align-content: center;
		justify-content: center;
	}
	
`
export class FlexControl extends React.Component<Props, {}> {

	static defaultProps = {
		flexbox: null,
		onclick: () => null,
		isSelected: false,
		childQuantity: 4,
	};

	protected getItems = () => {
		let elements = [];
		for (let i = 0; i < this.props.childQuantity; i++) {
			elements.push(
				<div key={i} className={"item"}>
					<span>{i + 1}</span>
				</div>,
			);
		}
		return elements;
	};

	protected handleClick = () => {
		this.props.onClick(this.props.property, this.props.value);
	};

	protected isSelected = () => {
		return this.props.flexbox[this.props.property] === this.props.value;
	};

	public render() {

		const {className} = this.props;

		let classNamesResult = classNames(
			"FlexControl",
			className,
			{
				box: true,
				boxSelected: this.isSelected(),
			},
		);

		return (
			<div onClick={this.handleClick}>
				<div className={classNamesResult}>
					{this.getItems()}
				</div>
				<div>{this.props.property && this.props[this.props.property]}</div>
				<small>{this.props.value}</small>
			</div>
		);
	}

}