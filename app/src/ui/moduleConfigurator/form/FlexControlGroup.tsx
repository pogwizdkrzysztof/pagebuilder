import classNames from "classnames";
import * as React from "react";
import {CSSProperties} from "react";
import {BaseProps} from "../../../App";
import {sc} from "../../../lib/StyledComponents";

interface Props extends BaseProps {
	title:string,
	property:string,
	flexbox:CSSProperties,
	help:string,
	onClick:(property:string, value:string | number) => void
}


@sc`
	display: flex;
`
export class FlexControlGroup extends React.Component<Props, {}> {

	static defaultProps = {
		title: '',
		help: '',
	};

	public render() {

		const {title, help, children, className, property, flexbox, onClick} = this.props;
		const childrenWithPropertyProp = React.Children.map(children, (child:React.ReactElement<any>) => {
				return React.cloneElement(child, {property, flexbox, onClick});
			},
		);

		return (
			<div>
				<h2>
					{title}
					{help && <small>{help}</small>}
				</h2>
				<div className={classNames("FlexControlGroup", className)}>
					{childrenWithPropertyProp}
				</div>
			</div>
		);
	}

}