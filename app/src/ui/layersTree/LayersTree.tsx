import * as React from "react";
import {MapStateToProps} from "react-redux";
import {OutlinesActions} from "../../actions/OutlinesActions";
import {StageActions} from "../../actions/StageActions";
import {BaseProps, BaseState} from "../../App";
import {combine} from "../../lib/redux/ReduxDecorators";
import {RootState} from "../../reducers/RootReducer";
import {StageSelectors} from "../../selectors/StageSelectors";
import {ModuleData} from "../../stage/modules/data/ModuleData";

interface StateProps {
	selectedModuleId:string,
	modules:{ [key:string]:ModuleData };
	mainModule:ModuleData;
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {

	const modules = StageSelectors.getById(state);
	const mainModuleData = StageSelectors.getMainModuleData(state);
	const selectedModuleId = StageSelectors.getSelectedModuleId(state);

	return {
		selectedModuleId: selectedModuleId,
		modules: modules,
		mainModule: mainModuleData,
	};
};

type ExternalProps = Partial<StateProps>;

interface Props extends BaseProps, ExternalProps {}

@combine(mapStateToProps)
export class LayersTree extends React.Component<Props, BaseState> {

	protected renderBranch(module:ModuleData, modules:{ [key:string]:ModuleData }, index:number = 0) {
		let children = module.submodulesIds.map((id, index) => {
			return this.renderBranch(modules[id], modules, index);
		});
		let subBranch = React.createElement("ul", {children});
		let isSelected = StageSelectors.getSelectedModuleId() === module.id;
		return <li key={index} className={isSelected ? "selected" : null}
				   onClick={(event) => {
					   event.stopPropagation();
					   StageActions.selectModule(module.id);
				   }}
				   onMouseOver={(event) => {
					   event.stopPropagation();
					   OutlinesActions.clear();
					   OutlinesActions.add(module.id);
				   }}
				   onMouseLeave={(event) => {
					   event.stopPropagation();
					   OutlinesActions.clear();
				   }}
		>
			<span>{module.module} ({module.id.toString().slice(0, 8)})</span>
			{subBranch}
		</li>;
	}

	public render() {
		return <div className={"LayersTree"}>
			<ul className={"list"}>
				{this.renderBranch(this.props.mainModule, this.props.modules)}
			</ul>
		</div>;
	}
}