import classNames from "classnames";
import * as React from "react";
import {MapStateToProps} from "react-redux";
import {AppUIActions} from "../../actions/AppUIActions";
import {StageActions} from "../../actions/StageActions";
import {UndoRedoActions} from "../../actions/UndoRedoActions";
import {BaseProps} from "../../App";
import {AppUITab} from "../../const/AppUITab";
import {combine} from "../../lib/redux/ReduxDecorators";
import {RootState} from "../../reducers/RootReducer";
import {ModulesState} from "../../reducers/states/ModulesState";
import {DebugPanel} from "../debug/DebugPanel";
import {LayersTree} from "../layersTree/LayersTree";
import {ModuleConfigurator} from "../moduleConfigurator/ModuleConfigurator";
import {ModulesList} from "../modulesList/ModulesList";
import {TODOPanel} from "../TODOPanel";
import {Icon} from "./../Icon";

interface StateProps {
	activeTab:AppUITab,
	modules:ModulesState,
	selectedModuleId:string,
	undoAllowed:boolean,
	redoAllowed:boolean
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {
	return {
		activeTab: state.appUI.activeTab,
		modules: state.modules,
		selectedModuleId: state.stage.selectedModuleId,
		undoAllowed: !!state.stage.past.length,
		redoAllowed: !!state.stage.future.length,
	};
};

type ExternalProps = Partial<StateProps>;

interface Props extends BaseProps, ExternalProps {}

@combine(mapStateToProps)
export class UISidebar extends React.Component<Props> {

	private ref = null;

	constructor(props) {
		super(props);
		this.state = {
			topOffset: 0,
		};
	}

	public render() {

		return (
			<div className={"UISidebar"}
				 ref={ref => this.ref = ref}
			>
				{this.props.activeTab &&
				 <div className={"UISidebarContent"}>
					 {this.props.activeTab == AppUITab.UI_MODULES_LIST_TAB &&
					  <ModulesList/>}
					 {this.props.activeTab == AppUITab.UI_CONFIG_TAB &&
					  <ModuleConfigurator/>}
					 {this.props.activeTab == AppUITab.UI_LAYERS_TAB &&
					  <LayersTree/>}
					 {this.props.activeTab == AppUITab.UI_DEBUG_TAB &&
					  <DebugPanel/>}

					 {[AppUITab.UI_MODULES_LIST_TAB, AppUITab.UI_CONFIG_TAB, AppUITab.UI_DEBUG_TAB, AppUITab.UI_LAYERS_TAB].indexOf(
						 this.props.activeTab) === -1 &&
					  <TODOPanel tabName={this.props.activeTab}/>
					 }
				 </div>}
				<div className={"UISidebarMenu"}>
					<div className={'section'}>
						<UISidebarMenuButton className={"button-undo"}
											 title={"Undo"}
											 icon={"undo"}
											 onClick={UndoRedoActions.undo}
											 isDisabled={!this.props.undoAllowed || this.props.activeTab === AppUITab.UI_CONFIG_TAB}
						/>
						<UISidebarMenuButton className={"button-redo"}
											 title={"Redo"}
											 icon={"redo"}
											 onClick={UndoRedoActions.redo}
											 isDisabled={!this.props.redoAllowed || this.props.activeTab === AppUITab.UI_CONFIG_TAB}
						/>
					</div>
					<div className={'section'}>
						<UISidebarMenuButton className={"button-modules"}
											 title={"Modules list"}
											 icon={"modules"}
											 onClick={() => AppUIActions.toggleTab(AppUITab.UI_MODULES_LIST_TAB)}
											 isActive={this.props.activeTab === AppUITab.UI_MODULES_LIST_TAB}
						/>
						<UISidebarMenuButton className={"button-config"}
											 title={"Config"}
											 icon={"config"}
											 onClick={() => {

												 if (this.props.selectedModuleId) {
													 StageActions.toggleEditModule(this.props.selectedModuleId);
												 }

												 // AppUIActions.toggleTab(AppUITab.UI_CONFIG_TAB);
												 // StageActions.toggleEditModule(this.props.selectedModuleId);
												 AppUIActions.toggleTab(AppUITab.UI_CONFIG_TAB);
											 }}
											 isActive={this.props.activeTab === AppUITab.UI_CONFIG_TAB}
						/>
						<UISidebarMenuButton className={"button-layers"}
											 title={"Layers"}
											 icon={"layers"}
											 onClick={() => AppUIActions.toggleTab(AppUITab.UI_LAYERS_TAB)}
											 isActive={this.props.activeTab === AppUITab.UI_LAYERS_TAB}
						/>
						<UISidebarMenuButton className={"button-settings"}
											 title={"Settings"}
											 icon={"settings"}
											 onClick={() => AppUIActions.toggleTab(AppUITab.UI_SETTINGS_TAB)}
											 isActive={this.props.activeTab === AppUITab.UI_SETTINGS_TAB}
						/>
					</div>
					<UISidebarMenuButton className={"button-preview"}
										 title={"Preview"}
										 icon={"preview"}
										 onClick={() => null}
										 isActive={this.props.activeTab === AppUITab.UI_PREVIEW_TAB}
					/>
					<UISidebarMenuButton className={"button-export"}
										 title={"Export"}
										 icon={"export"}
										 onClick={() => null}
										 isActive={this.props.activeTab === AppUITab.UI_EXPORT_TAB}
					/>
					<UISidebarMenuButton className={"button-debug"}
										 title={"Debug"}
										 icon={"debug"}
										 onClick={() => AppUIActions.toggleTab(AppUITab.UI_DEBUG_TAB)}
										 isActive={this.props.activeTab === AppUITab.UI_DEBUG_TAB}
					/>
				</div>
			</div>
		);
	}

}

const UISidebarMenuButton = ({className, icon, onClick, title = "", isActive = false, isDisabled = false}) => {
	const clazz = classNames(
		"UISidebarMenuButton",
		className,
		{disabled: isDisabled},
		{active: isActive},
	);
	return <button className={clazz}
				   disabled={isDisabled}
				   onClick={onClick}
				   title={title}
	>
		<Icon icon={icon}/>
	</button>;
};