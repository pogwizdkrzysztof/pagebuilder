export class ToolbarData {

	public moduleId:string;
	public moduleRect:ClientRect;

	constructor(moduleId:string, moduleRect:ClientRect) {
		this.moduleId = moduleId;
		this.moduleRect = moduleRect;
	}

}