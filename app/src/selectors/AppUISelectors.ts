import {AppUITab} from "../const/AppUITab";
import {ScrollPosition} from "../lib/ScrollPosition";
import {M} from "../models/M";
import {RootState} from "../reducers/RootReducer";

export class AppUISelectors {

	public static getScrollPosition(state:RootState = M.store.getState()):ScrollPosition {
		return state.appUI.scrollPosition;
	}

	public static getActiveTab(state:RootState = M.store.getState()):AppUITab {
		return state.appUI.activeTab;
	}

}