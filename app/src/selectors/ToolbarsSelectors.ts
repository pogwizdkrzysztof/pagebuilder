import {RootState} from "../reducers/RootReducer";

export class ToolbarsSelectors {

	public static getAllIds(state:RootState) {
		return state.stageUI.toolbars.allIds;
	}

	public static getById(state:RootState) {
		return state.stageUI.toolbars.byId;
	}

}
