import {AppStatus} from "../const/AppStatus";
import {RootState} from "../reducers/RootReducer";

export class AppSelectors {

	public static getStatus(state:RootState):AppStatus {
		return state.app.status;
	}

}