import {DropSide} from "../dnd/const/DropSide";
import {RootState} from "../reducers/RootReducer";

export class DnDSelectors {

	public static isDragging(state:RootState):boolean {
		return state.stageUI.dragAndDrop.isDragging;
	}

	public static getDropSide(state:RootState):DropSide {
		return state.stageUI.dragAndDrop.dropSide;
	}

	public static getDragModuleId(state:RootState):string {
		return state.stageUI.dragAndDrop.dragModuleId;
	}

	public static getDropOverModuleId(state:RootState):string {
		return state.stageUI.dragAndDrop.dropOverModuleId;
	}

	public static getDropOverModuleRect(state:RootState):ClientRect {
		return state.stageUI.dragAndDrop.dropOverModuleRect;
	}

	public static getDropModuleId(state:RootState):string {
		return state.stageUI.dragAndDrop.dropModuleId;
	}

}