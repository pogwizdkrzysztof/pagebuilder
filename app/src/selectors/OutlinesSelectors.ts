import {RootState} from "../reducers/RootReducer";

export class OutlinesSelectors {

	public static getAllIds(state:RootState) {
		return state.stageUI.outlines.allIds;
	}

	public static getById(state:RootState) {
		return state.stageUI.outlines.byId;
	}

}
