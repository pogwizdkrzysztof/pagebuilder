import {RootState} from "../reducers/RootReducer";
import {ModulesState} from "../reducers/states/ModulesState";

export class ModulesSelectors {

	public static getModules(state:RootState):ModulesState {
		return state.modules;
	}

}