import values from "lodash/values";
import {M} from "../models/M";
import {RootState} from "../reducers/RootReducer";
import {MainData} from "../stage/modules/data/MainData";
import {ModuleData} from "../stage/modules/data/ModuleData";

export class StageSelectors {

	public static getSelectedModuleId(state:RootState = M.store.getState()):string {
		return state.stage.selectedModuleId;
	}

	public static getSelectedModuleData(state:RootState = M.store.getState()):ModuleData {
		let stage = state.stage;
		return stage.present.byId[stage.selectedModuleId];
	}

	public static getModuleData(moduleDataId:string, state:RootState = M.store.getState()):ModuleData {
		return state.stage.present.byId[moduleDataId];
	}

	public static getMainModuleData(state:RootState = M.store.getState()):MainData {
		const mainModuleId = state.stage.present.allIds[0];
		return StageSelectors.getModuleData(mainModuleId, state) as MainData;
	}

	public static getModulesDatas(modulesDatasIds:string[], state:RootState = M.store.getState()):ModuleData[] {
		return modulesDatasIds.map((id) => StageSelectors.getModuleData(id, state));
	}

	public static getTemporaryModuleData(moduleDataId:string, state:RootState = M.store.getState()):ModuleData {
		return state.stage.temporary.byId[moduleDataId];
	}

	public static getTemporaryModules(state:RootState = M.store.getState()):ModuleData[] {
		const temporaryModules = state.stage.temporary.byId;
		return values(temporaryModules);
	}

	public static getAllIds(state:RootState) {
		return state.stage.present.allIds;
	}

	public static getById(state:RootState) {
		return state.stage.present.byId;
	}

	public static getPast(state:RootState) {
		return state.stage.past;
	}

	public static getFuture(state:RootState) {
		return state.stage.future;
	}

	public static isInConfigMode(state:RootState = M.store.getState()):boolean {
		return !!state.stage.temporary.allIds.length;
	}
}
