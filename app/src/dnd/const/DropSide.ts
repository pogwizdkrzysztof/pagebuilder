export enum DropSide {
	ABOVE = "ABOVE",
	AFTER = "AFTER",
	BELOW = "BELOW",
	BEFORE = "BEFORE",
	MIDDLE = "MIDDLE", // Y-center
	CENTER = "CENTER" // X-center
}
