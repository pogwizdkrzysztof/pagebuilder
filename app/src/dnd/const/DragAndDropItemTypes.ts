export enum DragAndDropItemTypes {
	PLAIN_MODULE = "PLAIN_MODULE",
	STAGE_MODULE = "STAGE_MODULE"
}