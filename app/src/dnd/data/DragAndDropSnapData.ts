import {DropSide} from "../const/DropSide";

export class DragAndDropSnapData {
	public dropSide:DropSide;
	public isNearTheEdge:boolean;

	constructor(dropSide:DropSide, isNearTheEdge:boolean) {
		this.dropSide = dropSide;
		this.isNearTheEdge = isNearTheEdge;
	}
}
