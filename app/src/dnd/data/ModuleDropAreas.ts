export class ModuleDropAreas {
	public top:number;
	public right:number;
	public bottom:number;
	public left:number;

	constructor(top:number = 50, right:number = 50, bottom:number = 50, left:number = 50) {
		this.top = top;
		this.right = right;
		this.bottom = bottom;
		this.left = left;
	}
}
