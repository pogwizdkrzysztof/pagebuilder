import {Point} from "../../lib/Point";
import {DragAndDropState} from "../../reducers/DragAndDropReducer";
import {ModuleData} from "../../stage/modules/data/ModuleData";
import {ModuleType} from "../../stage/modules/ModuleType";
import {ModulesUtil} from "../../stage/modules/utils/ModulesUtil";
import {DropAxis} from "../const/DropAxis";
import {DropSide} from "../const/DropSide";
import {DragAndDropSnapData} from "../data/DragAndDropSnapData";
import {ModuleDropAreas} from "../data/ModuleDropAreas";


export class DnDUtil {

	public static getDropAxis(dropSide:DropSide):DropAxis {
		switch (dropSide) {
			case DropSide.ABOVE:
			case DropSide.BELOW:
				return DropAxis.X;
			case DropSide.BEFORE:
			case DropSide.AFTER:
				return DropAxis.Y;
		}
	}

	public static isDropStageModuleAllowed(module:ModuleData, parentModule:ModuleData,
		allModules:{ [key:string]:ModuleData }):boolean {
		if (module === parentModule)
			return false;
		if (DnDUtil.isDroppedIntoOwnSubmodule(module, parentModule, allModules))
			return false;
		return DnDUtil.isDropModuleAllowed(module.module, parentModule);
	}

	public static isDropModuleAllowed(moduleType:ModuleType, parentModule:ModuleData):boolean {
		const modulePolicy = ModulesUtil.getPolicy(parentModule);

		if (modulePolicy.allowedSubmodules === null)
			return false;

		// Filtering allowed
		if (parentModule &&
			(modulePolicy.allowedSubmodules !== null) &&
			(modulePolicy.allowedSubmodules.length !== 0) &&
			(modulePolicy.allowedSubmodules.indexOf(moduleType) === -1))
		{
			return false;
		}

		// Filtering disallowed
		if (parentModule &&
			(modulePolicy.disallowedSubmodules !== null) &&
			(modulePolicy.disallowedSubmodules.length !== 0) &&
			(modulePolicy.disallowedSubmodules.indexOf(moduleType) !== -1))
		{
			return false;
		}
		return true;
	}

	public static isDroppedIntoOwnSubmodule(module:ModuleData, parentModule:ModuleData,
		allModules:{ [key:string]:ModuleData }):boolean {
		if (parentModule &&
			parentModule.parentId !== null &&
			parentModule.parentId === module.id)
			return true;

		if (parentModule &&
			(parentModule.parentId === null || !parentModule))
			return false;

		return DnDUtil.isDroppedIntoOwnSubmodule(module, allModules[parentModule.parentId], allModules);
	}

	public static getSnapData(mousePosition:Point, clientRect:ClientRect, dropPlaneValues:ModuleDropAreas,
		nearTollerance = 0, dropAxis:DropAxis):DragAndDropSnapData {
		let dropSide = null,
			isNearTheEdge = false;

		if (dropAxis == DropAxis.X) {
			if (mousePosition.x <= clientRect.left + Math.floor((clientRect.width * dropPlaneValues.left) / 100)) {
				dropSide = DropSide.BEFORE;
				isNearTheEdge = (mousePosition.x <= clientRect.left + nearTollerance);
			}
			if (mousePosition.x >= clientRect.right - Math.floor((clientRect.width * dropPlaneValues.right) / 100)) {
				dropSide = DropSide.AFTER;
				isNearTheEdge = (mousePosition.x >= clientRect.right - nearTollerance);
			}
		}

		if (dropAxis == DropAxis.Y) {
			if (mousePosition.y <= clientRect.top + Math.floor((clientRect.height * dropPlaneValues.top) / 100)) {
				dropSide = DropSide.ABOVE;
				isNearTheEdge = (mousePosition.y <= clientRect.top + nearTollerance);
			}
			if (mousePosition.y >= clientRect.bottom - Math.floor(
				(clientRect.height * dropPlaneValues.bottom) / 100))
			{
				dropSide = DropSide.BELOW;
				isNearTheEdge = (mousePosition.y >= clientRect.bottom - nearTollerance);
			}
		}

		return new DragAndDropSnapData(dropSide, isNearTheEdge);
	}

	public static getDropAxisFromModule(moduleData):DropAxis {
		const modulePolicy = ModulesUtil.getPolicy(moduleData);
		return modulePolicy.getDropAxis(moduleData) || DropAxis.Y;
	}

	public static getDropIndex(dropModuleDataIndex:number, dropSide:DropSide):number {
		let newDropIndex = -1;

		switch (dropSide) {
			case DropSide.AFTER:
			case DropSide.BELOW:
				newDropIndex = dropModuleDataIndex + 1;
				break;
			case DropSide.BEFORE:
			case DropSide.ABOVE:
				newDropIndex = dropModuleDataIndex;
		}

		return Math.max(0, newDropIndex);
	}


	public static hasDndChanged(dndValue:DragAndDropState, dndOther:DragAndDropState) {
		return (
			dndValue.dropSide !== dndOther.dropSide ||
			dndValue.dropOverModuleId !== dndOther.dropOverModuleId ||
			dndValue.dropModuleId !== dndOther.dropModuleId
		);
	}

	public static getNearestDropModule(modules:{ [key:string]:ModuleData }, fromModuleId:string, moduleId:string,
		moduleType:ModuleType, deep:number = 3)
	{

		const droppingStageModule = modules[moduleId];

		const findNearestRecursive = (moduleData:ModuleData) => {

			if (droppingStageModule) {
				if (DnDUtil.isDropStageModuleAllowed(droppingStageModule, moduleData, modules)) {
					return moduleData.id;
				}
			}
			else {
				if (DnDUtil.isDropModuleAllowed(moduleType, moduleData)) {
					return moduleData.id;
				}
			}

			const parentModule = modules[moduleData.parentId];

			if (parentModule) {
				return findNearestRecursive(parentModule);
			}
			return null;
		};

		const moduleData = modules[fromModuleId];
		if (!moduleData) {
			throw new Error(`module ${fromModuleId} does not exists`);
		}

		return findNearestRecursive(moduleData);

	}

}