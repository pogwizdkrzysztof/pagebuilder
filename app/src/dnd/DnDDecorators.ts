import {
	DragSource,
	DragSourceCollector,
	DragSourceSpec,
	DropTarget,
	DropTargetCollector,
	DropTargetMonitor,
	DropTargetSpec,
} from 'react-dnd';
import * as ReactDOM from "react-dom";
import {DragAndDropActions} from "../actions/DragAndDropActions";
import {ToolbarsActions} from "../actions/ToolbarsActions";
import {Point} from "../lib/Point";
import {M} from "../models/M";
import {DragAndDropState} from "../reducers/DragAndDropReducer";
import {DragItem} from "../stage/modules/DragItem";
import {ModuleDnDProps, ModuleProps, ModuleState, ModuleView} from "../stage/modules/view/ModuleView";
import {DragAndDropItemTypes} from "./const/DragAndDropItemTypes";
import {DropSide} from "./const/DropSide";
import {ModuleDropAreas} from "./data/ModuleDropAreas";
import {DnDUtil} from "./utils/DnDUtil";


const stageModuleSource:DragSourceSpec<ModuleProps, DragItem> = {
	beginDrag(props:ModuleProps):DragItem {

		// StageActions.saveModules();
		DragAndDropActions.beginDrag();
		// ToolbarsActions.clear();
		// OutlinesActions.clear();

		const moduleData = props.moduleData;
		const dragItem = {
			itemType: DragAndDropItemTypes.STAGE_MODULE,
			id: moduleData.id,
			moduleType: moduleData.module,
			name: moduleData.name,
			icon: moduleData.icon,
		} as DragItem;

		return dragItem;
	},
	endDrag(props:ModuleProps) {
		DragAndDropActions.endDrag();
		ToolbarsActions.clear();
		DragAndDropActions.updateDropInfo();
	},
};

const stageModuleTarget:DropTargetSpec<ModuleProps> = {
	drop(props:ModuleProps, monitor:DropTargetMonitor, component:ModuleView<ModuleProps, ModuleState>) {

		const dragModule = monitor.getItem() as DragItem;
		const dndData = M.store.getState().stageUI.dragAndDrop;

		if (!monitor.didDrop()) {
			DragAndDropActions.dropStageModule(
				dragModule.itemType,
				dragModule.id,
				dragModule.moduleType,
				dndData.dropSide,
				dndData.dropOverModuleId,
				dndData.dropModuleId,
			);
		}

	},
	/**
	 * Hover przechodzi po elementach na wzór "Capturing mode" w eventach.
	 * Czyli od najstarszego modułu (parenta) - do najmłodszego (na którym mamy hover - currentElement)
	 *
	 * Jeli aktualnie procesowanym elementem jest moduł na którym mamy hover (currentElement),
	 * to sprawdzamy czy możemy na niego dropnąć.
	 *        Jeśli tak, to sprawdzamy czy jesteśmy blisko krawędzi,
	 *            Jeśli tak, to sprawdzamy czy możemy dropnąć na parenta.
	 *                Jeśli tak, to zapisujemy do modelu pełne informacje o tym elemencie i kończymy.
	 *            Jeśli nie, to zapisujemy do modelu pełne informacje o currentElement i kończymy.
	 * Jeśli nie, to sprawdzamy czy możemy dropnąć na parenta
	 *        Jeśli tak, to zapisujemy pełne informacje o parencie i kończymy.
	 * Jeśli nie, to zapisujemy element na który możemy dropnąć (pełne informacje uzupełnimy w kolejnym przebiegu)
	 *
	 * Jeśli aktualnie procesowanym elementem jest element który zapisaliśmy wcześniej w modelu
	 *        to uzupełniamy dane w modelu (jeśli się różnią)
	 *
	 * @param props
	 * @param monitor
	 * @param component
	 */
	hover(props:ModuleProps, monitor:DropTargetMonitor, component:ModuleView<ModuleProps, ModuleState>) {

		const dragItem = monitor.getItem() as DragItem;

		const mouseWindowPosition = monitor.getClientOffset() as Point;

		const currentModuleId = props.moduleData.id;
		const parentModuleId = props.moduleData.parentId;

		const modules = M.store.getState().stage.present.byId;
		const dndData = M.store.getState().stageUI.dragAndDrop;

		const isOverCurrent = monitor.isOver({shallow: true});
		const isOverChildOfDropModule = parentModuleId === dndData.dropModuleId;

		let ref = null;
		let dropRect = null;

		let dropAreas = null;
		let nearTollerance = null;

		let dropAxis = null;
		let snapData = null;
		let dropSide = null;

		if (isOverCurrent || isOverChildOfDropModule) {
			ref = ReactDOM.findDOMNode(component) as Element;
			dropRect = ref.getBoundingClientRect();

			dropAreas = new ModuleDropAreas();
			nearTollerance = 8;

			dropAxis = DnDUtil.getDropAxisFromModule(props.moduleData);
			snapData = DnDUtil.getSnapData(mouseWindowPosition, dropRect, dropAreas, nearTollerance, dropAxis);
			dropSide = snapData.dropSide;
		}


		if (isOverCurrent) {

			let nearestDropModuleId = DnDUtil.getNearestDropModule(
				modules,
				currentModuleId,
				dragItem.id,
				dragItem.moduleType,
			);

			let canDropToCurrent = nearestDropModuleId === currentModuleId;
			let canDropToParent = nearestDropModuleId === parentModuleId;

			if (canDropToCurrent) {

				if (snapData.isNearTheEdge && parentModuleId) {
					let secondNearestDropModuleId = DnDUtil.getNearestDropModule(
						modules,
						parentModuleId,
						dragItem.id,
						dragItem.moduleType,
					);
					let canAlsoDropToParent = parentModuleId === secondNearestDropModuleId;
					if (canAlsoDropToParent) {
						let newDndData:DragAndDropState = {
							dropSide: dropSide,
							dropOverModuleId: currentModuleId,
							dropModuleId: secondNearestDropModuleId,
						};

						if (DnDUtil.hasDndChanged(dndData, newDndData)) {
							DragAndDropActions.updateDropInfo(
								dropSide,
								dropRect,
								currentModuleId,
								secondNearestDropModuleId,
								dragItem.id,
							);
						}
						return;
					}
				}

				let hasChildren = !!props.moduleData.submodulesIds.length;
				if (!snapData.isNearTheEdge && !hasChildren) {
					dropSide = DropSide.MIDDLE;
				}

				let newDndData:DragAndDropState = {
					dropSide: dropSide,
					dropOverModuleId: currentModuleId,
					dropModuleId: nearestDropModuleId,
				};

				if (DnDUtil.hasDndChanged(dndData, newDndData)) {
					DragAndDropActions.updateDropInfo(
						dropSide,
						dropRect,
						currentModuleId,
						nearestDropModuleId,
						dragItem.id,
					);
					return;
				}

			}
			else if (canDropToParent) {

				let parentModule = modules[parentModuleId];
				let dropAxis = DnDUtil.getDropAxisFromModule(parentModule);
				let snapData = DnDUtil.getSnapData(mouseWindowPosition, dropRect, dropAreas, nearTollerance,
					dropAxis);
				let dropSide = snapData.dropSide;

				let newDndData:DragAndDropState = {
					dropSide: dropSide,
					dropOverModuleId: currentModuleId,
					dropModuleId: nearestDropModuleId,
				};

				if (DnDUtil.hasDndChanged(dndData, newDndData)) {
					DragAndDropActions.updateDropInfo(
						dropSide,
						dropRect,
						currentModuleId,
						nearestDropModuleId,
						dragItem.id,
					);
				}
				return;

			}
			else { //setting up other nearest module

				let newDndData:DragAndDropState = {
					dropSide: dndData.dropSide,
					dropOverModuleId: dndData.dropOverModuleId,
					dropModuleId: nearestDropModuleId,
				};

				if (DnDUtil.hasDndChanged(dndData, newDndData)) {
					DragAndDropActions.updateDropInfo(
						newDndData.dropSide,
						dndData.dropOverModuleRect,
						newDndData.dropOverModuleId,
						nearestDropModuleId,
						dragItem.id,
					);
				}
				return;
			}

		}
		else if (isOverChildOfDropModule) {

			let newDndData:DragAndDropState = {
				dropSide: dropSide,
				dropOverModuleId: currentModuleId,
				dropModuleId: dndData.dropModuleId,
			};

			if (DnDUtil.hasDndChanged(dndData, newDndData)) {
				DragAndDropActions.updateDropInfo(
					dropSide,
					dropRect,
					currentModuleId,
					dndData.dropModuleId,
					dragItem.id,
				);
			}

		}

	},
	canDrop(props:ModuleProps, monitor:DropTargetMonitor):boolean {

		if (monitor.didDrop())
			return false;

		const isOver = monitor.isOver({shallow: true});
		if (!isOver)
			return false;

		const dragModule = monitor.getItem() as DragItem;
		const moduleData = props.moduleData;

		if (dragModule.id === moduleData.id) // drop on the same element
			return false;

		return true;

	},
};

const sourceCollect:DragSourceCollector<ModuleDnDProps, null> = function (connect, monitor):Partial<ModuleDnDProps> {
	return {
		connectDragSource: connect.dragSource(),
		isDragging: monitor.isDragging(),
		connectDragPreview: connect.dragPreview(),
	};
};

const targetCollect:DropTargetCollector<ModuleDnDProps, null> = function (connect, monitor):Partial<ModuleDnDProps> {
	return {
		connectDropTarget: connect.dropTarget(),
		isOver: monitor.isOver({shallow: true}),
	};
};


export function ModuleDropTarget(constructor) {
	let decoratedConstructor = (DropTarget(
		[DragAndDropItemTypes.PLAIN_MODULE, DragAndDropItemTypes.STAGE_MODULE],
		stageModuleTarget,
		targetCollect,
	)(constructor)) as any;
	return decoratedConstructor;
}

export function ModuleDragSource(constructor) {
	let decoratedConstructor = (DragSource(
		DragAndDropItemTypes.STAGE_MODULE,
		stageModuleSource,
		sourceCollect,
	)(constructor)) as any;
	return decoratedConstructor;
}

export function ModuleDragAndDrop(constructor) {
	let decoratedConstructor = ModuleDropTarget(constructor);
	decoratedConstructor = ModuleDragSource(decoratedConstructor);
	return decoratedConstructor;
}

export function ModuleListItemDragSource(moduleSource, sourceCollect) {
	return function (constructor) {
		let decoratedConstructor = (DragSource(
			DragAndDropItemTypes.PLAIN_MODULE,
			stageModuleSource,
			sourceCollect,
		)(constructor)) as any;
		return decoratedConstructor;
	};
}