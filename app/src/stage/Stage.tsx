import classNames from "classnames";
import * as React from "react";
import {connect, MapStateToProps} from "react-redux";
import {BaseProps, BaseState} from "../App";
import {RootState} from "../reducers/RootReducer";
import {StageSelectors} from "../selectors/StageSelectors";
import {StageModuleView} from "./modules/view/StageModuleView";

interface Props extends BaseProps, Partial<StateProps> {}

interface StateProps {
	stageModulesIds:string[],
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {
	return {
		stageModulesIds: StageSelectors.getAllIds(state),
	};
};


@(connect<StateProps, {}, Props, BaseState>(mapStateToProps) as any)
export class Stage extends React.Component<Props, BaseState> {

	public componentDidMount() {
		window.addEventListener('resize', this._forceUpdate);
	}

	public componentWillUnmount() {
		window.removeEventListener('resize', this._forceUpdate);
	}

	protected _forceUpdate = () => {
		this.forceUpdate();
	};

	protected getMainView = () => {
		const moduleDataId = this.props.stageModulesIds[0];
		return <StageModuleView key={moduleDataId}
								moduleDataId={moduleDataId}
		/>;
	};

	public render() {
		const element = this.getMainView();
		const stageClassNames = classNames({
			"Stage": true
		});
		return (<div className={stageClassNames}>
			{element}
		</div>);
	}

}