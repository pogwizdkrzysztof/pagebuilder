import {DragAndDropItemTypes} from "../../dnd/const/DragAndDropItemTypes";
import {ModuleType} from "./ModuleType";

export type DragItem = {
	itemType:DragAndDropItemTypes;
	id:string;
	moduleType:ModuleType;
	name:string;
	icon:string;
}