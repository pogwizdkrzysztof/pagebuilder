import * as uuid from "uuid";
import {ObjectUtil} from "../../../lib/utils/ObjectUtil";
import {M} from "../../../models/M";
import {ModulesModel} from "../../../models/ModulesModel";
import {ModuleData} from "../data/ModuleData";
import {ModuleDataPolicy} from "../policy/ModuleDataPolicy";

export class ModulesUtil {

	public static regenerateId(module:ModuleData, id?:string) {
		if (id)
			module.id = id;
		else
			module.id = uuid.v4();
	}

	public static setParent(child:ModuleData, parent:ModuleData, submoduleIndex:number = parent.submodulesIds.length) {
		child.parentId = parent.id;
		parent.submodulesIds.splice(submoduleIndex, 0, child.id);
	}

	public static addSubmodule(parent:ModuleData, child:ModuleData, submoduleIndex?:number) {
		ModulesUtil.setParent(child, parent, submoduleIndex);
	}

	public static removeSubmodule(parent:ModuleData, submoduleId:string) {
		let moduleIdIndex = parent.submodulesIds.indexOf(submoduleId);
		if (moduleIdIndex != -1)
			parent.submodulesIds.splice(moduleIdIndex, 1);
	}

	public static getPolicy(module:ModuleData):ModuleDataPolicy {
		return ModulesModel.policies[module.module];
	}

	// ---------
	public static _getModuleStructure(module:ModuleData,
		modules:{ [key:string]:ModuleData }):{ name:string, children:any[] } {
		let obj = {
			name: module.module,
			children: [],
		};
		module.submodulesIds.forEach((id) => {
			obj.children.push(ModulesUtil._getModuleStructure(modules[id], modules));
		});
		return obj;
	}

	public static getTree(module:ModuleData, modules:{ [key:string]:ModuleData }) {
		var modulesTree = {};
		module.submodulesIds.forEach((moduleId) => {
			modulesTree = ModulesUtil._getModuleStructure(module, modules);
		});
		return modulesTree;
	}

	// ---------
	public static getList(module:ModuleData, modules:{ [key:string]:ModuleData }):string[] {
		let modulesList:string[] = [];
		modulesList.push(module.id);
		module.submodulesIds.forEach((moduleId) => {
			modulesList.push(...ModulesUtil.getList(modules[moduleId], modules));
		});
		return modulesList;
	}

	public static deepClone(clonedModule:ModuleData, modules:{ [key:string]:ModuleData }):ModuleData[] {
		let modulesList:ModuleData[] = [];
		let submodulesIds = clonedModule.submodulesIds;
		clonedModule.submodulesIds = [];

		modulesList.push(clonedModule);
		submodulesIds.forEach((moduleId) => {
			let clonedSubmodule = ObjectUtil.clone<ModuleData>(modules[moduleId]);
			ModulesUtil.regenerateId(clonedSubmodule);
			ModulesUtil.setParent(clonedSubmodule, clonedModule);
			modulesList.push(...ModulesUtil.deepClone(clonedSubmodule, modules));
		});
		return modulesList;
	}

	public static arrayToById(modules:ModuleData[]):{ [key:string]:ModuleData } {
		let byId = {};
		modules.forEach((module) => {
			byId[module.id] = module;
		});
		return byId;
	}


	public static getModulesById(modulesIds:string[],
		source:{ [key:string]:ModuleData } = M.store.getState().stage.present.byId):ModuleData[] {
		let moduleDatas:ModuleData[] = [];
		modulesIds.forEach((id) => {
			const moduleData = source[id];
			if (moduleData) {
				moduleDatas.push(moduleData);
			}
			else {
				throw new Error(`ModuleData with id: ${id}, does not exists`);
			}
		});
		return moduleDatas;
	}

}



