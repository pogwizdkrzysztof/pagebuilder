import * as React from "react";
import {M} from "../../../models/M";
import {ModuleData} from "../data/ModuleData";
import {StageModuleView} from "../view/StageModuleView";

export class ModuleViewUtil {

	public static getChildren(parentId:string,
		moduleDatas:{ [key:string]:ModuleData } = M.store.getState().stage.present.byId)
	{
		const parentModule = moduleDatas[parentId];
		const children = parentModule.submodulesIds.map((moduleDataId) => {
			const stageView = StageModuleView;
			const props = {
				key: moduleDataId,
				moduleDataId: moduleDataId,
			};
			return React.createElement(stageView, props);
		});
		return children;
	}

}