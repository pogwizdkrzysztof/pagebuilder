import {ColumnData} from "./data/ColumnData";
import {IModule} from "./Module";
import {ModuleType} from "./ModuleType";

export class ColumnModule implements IModule {
	public id = ModuleType.COLUMN;
	public name = "Column";
	public icon = "column";

	public getModuleData() {
		return [new ColumnData()];
	}
}