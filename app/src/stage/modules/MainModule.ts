import {MainData} from "./data/MainData";
import {IModule} from "./Module";
import {ModuleType} from "./ModuleType";

export class MainModule implements IModule {
	public id = ModuleType.MAIN;
	public name = "Main";
	public icon = "main";

	public getModuleData() {
		return [new MainData()];
	}
}