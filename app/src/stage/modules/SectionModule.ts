import {SectionData} from "./data/SectionData";
import {IModule} from "./Module";
import {ModuleType} from "./ModuleType";

export class SectionModule implements IModule {
	public id = ModuleType.SECTION;
	public name = "Section";
	public icon = "section";

	public getModuleData() {
		return [new SectionData()];
	}
}