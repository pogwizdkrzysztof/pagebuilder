import * as React from "react";
import {DropPlaceholder} from "../../../ui/DropPlaceholder";
import {TitledSectionData} from "../data/TitledSectionData";
import {ModuleProps, ModuleView} from "./ModuleView";

interface Props extends ModuleProps {
	moduleData:TitledSectionData
}

export class TitledSectionView extends ModuleView<Props> {

	public render() {
		return <div className={"TitledSectionView"}>
			{this.children || <DropPlaceholder/>}
		</div>;
	}

}