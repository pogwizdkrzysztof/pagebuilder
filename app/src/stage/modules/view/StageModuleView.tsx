import * as React from "react";
import {ComponentClass} from "react";
import {getEmptyImage} from "react-dnd-html5-backend";
import * as ReactDOM from "react-dom";
import {MapStateToProps} from "react-redux";
import {OutlinesActions} from "../../../actions/OutlinesActions";
import {StageActions} from "../../../actions/StageActions";
import {ToolbarsActions} from "../../../actions/ToolbarsActions";
import {AppUITab} from "../../../const/AppUITab";
import {ModuleDragAndDrop} from "../../../dnd/DnDDecorators";
import {combine} from "../../../lib/redux/ReduxDecorators";
import {M} from "../../../models/M";
import {ModulesModel} from "../../../models/ModulesModel";
import {RootState} from "../../../reducers/RootReducer";
import {AppUISelectors} from "../../../selectors/AppUISelectors";
import {StageSelectors} from "../../../selectors/StageSelectors";
import {ModulesUtil} from "../utils/ModulesUtil";
import {ModuleViewUtil} from "../utils/ModuleViewUtil";
import {ModuleProps, ModuleState, ModuleStateProps} from "./ModuleView";

const mapStateToProps:MapStateToProps<ModuleStateProps, ModuleProps, RootState> = (state,
	ownProps):ModuleStateProps => {
	const stage = state.stage;
	const moduleDataId = ownProps.moduleDataId;

	const isInConfig = !!stage.temporary.byId[moduleDataId];

	const moduleData = isInConfig
		? stage.temporary.byId[moduleDataId]
		: stage.present.byId[moduleDataId];

	const isSelected = !!(stage.selectedModuleId === moduleData.id);

	const updateModuleOutline = state.stageUI.outlines.allIds.indexOf(moduleDataId) !== -1 &&
								state.stageUI.outlines.byId[moduleDataId] === null;

	return {
		moduleData: moduleData,
		showId: state.debug.showIds,
		isInConfig: isInConfig,
		isSelected: isSelected,
		appUIActiveTab: state.appUI.activeTab, //@todo: passing activeTab for all modules isn't good idea
		updateModuleOutline: updateModuleOutline,
	};
};


@combine(mapStateToProps)
@ModuleDragAndDrop
export class StageModuleView extends React.Component<ModuleProps, ModuleState> {

	public ref = null;

	public componentDidMount() {
		if (this.props.connectDragPreview && ModulesUtil.getPolicy(this.props.moduleData).isDraggable) {
			this.props.connectDragPreview(getEmptyImage(), {
				captureDraggingState: true,
			});
		}
		this.ref.addEventListener('mouseover', this.handleMouseEnter);
		this.ref.addEventListener('mouseleave', this.handleMouseLeave);
		this.ref.addEventListener('click', this.handleClick);
		window.addEventListener('resize', this.updateToolbar);
	}

	public componentWillUnmount():void {
		this.ref.removeEventListener('mouseover', this.handleMouseEnter);
		this.ref.removeEventListener('mouseleave', this.handleMouseLeave);
		this.ref.removeEventListener('click', this.handleClick);
		window.removeEventListener('resize', this.updateToolbar);
	}

	public componentDidUpdate(prevProps:Readonly<ModuleProps>, prevState:Readonly<ModuleState>, snapshot?:any):void {
		this.updateToolbar();
		if (this.props.updateModuleOutline) {
			this.addOutline();
		}
	}

	protected updateToolbar = ():void => {
		if (this.props.isSelected) {
			this.addToolbar();
		}
	};

	protected handleMouseEnter = (event) => {
		event.stopPropagation();
		this.addOutline();
	};

	protected handleMouseLeave = (event) => {
		event.stopPropagation();
		this.removeOutline();
	};

	protected handleClick = (event) => {
		event.stopPropagation();
		if (this.props.isSelected) {
			return;
		}
		else {
			if (StageSelectors.isInConfigMode()) {
				let result = window.confirm("Do you really want to reject changes?");
				if (!result) {
					return;
				}
			}
		}

		if (AppUISelectors.getActiveTab() === AppUITab.UI_CONFIG_TAB) {
			StageActions.toggleEditModule(this.props.moduleDataId);
		}
		StageActions.selectModule(this.props.moduleDataId);
	};

	protected addOutline = () => {
		let moduleData = this.props.moduleData;
		OutlinesActions.clear();
		OutlinesActions.add(moduleData.id, this.getRect());
	};

	protected removeOutline = () => {
		OutlinesActions.remove(this.props.moduleData.id);
	};

	protected removeToolbar = () => {
		ToolbarsActions.clear();
	};

	protected addToolbar = () => {
		//@todo: add toolbarData
		ToolbarsActions.clear();
		ToolbarsActions.add(this.props.moduleDataId, this.ref);
	};

	private getRect = ():ClientRect => {
		const rect = this.ref.getBoundingClientRect();
		let scrollPos = AppUISelectors.getScrollPosition();
		const scrollLeft = scrollPos.scrollLeft;
		const scrollTop = scrollPos.scrollTop;
		return {
			top: rect.top + scrollTop,
			left: rect.left + scrollLeft,
			width: rect.width,
			height: rect.height,
		} as ClientRect;
	};

	private connectDnD = (node:Element) => {
		if (this.props.connectDragSource)
			this.props.connectDragSource(node);
		if (this.props.connectDropTarget)
			this.props.connectDropTarget(node as any);
	};

	public render() {
		const {moduleDataId} = this.props;
		const modulesById = !this.props.isInConfig
			? M.store.getState().stage.present.byId
			: M.store.getState().stage.temporary.byId;

		const moduleData = modulesById[moduleDataId];
		const moduleView = ModulesModel.views[moduleData.module] as ComponentClass<ModuleProps>;
		const children = ModuleViewUtil.getChildren(moduleDataId, modulesById);

		const props = {
			...this.props,
			ref: instance => {
				const node = ReactDOM.findDOMNode(instance) as Element;
				this.ref = node;
				this.connectDnD(node);
			},
		};

		return React.createElement<ModuleProps>(moduleView, props, children);
	}
}