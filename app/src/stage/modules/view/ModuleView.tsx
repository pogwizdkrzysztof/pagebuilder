import * as React from "react";
import {ReactNode} from "react";
import {ConnectDragPreview, ConnectDragSource, ConnectDropTarget} from "react-dnd";
import {BaseProps, BaseState} from "../../../App";
import {AppUITab} from "../../../const/AppUITab";
import {IModuleData} from "../data/ModuleData";

type ExternalModuleProps = Partial<ModuleStateProps>;
type ExternalDnDProps = Partial<ModuleDnDProps>;
type ExternalProps = ExternalModuleProps & ExternalDnDProps;

export interface ModuleProps extends BaseProps, ExternalProps {
	moduleDataId?:string;
	createElement?:Function;
	moduleView?:React.ComponentClass;
}

export interface ModuleStateProps {
	moduleData?:IModuleData;
	showId?:boolean,
	isInConfig?:boolean,
	isSelected?:boolean,
	appUIActiveTab?:AppUITab,
	updateModuleOutline:boolean
}

export interface ModuleDnDProps {
	isDragging?:boolean;
	isOver?:boolean;
	canDrop?:Function;
	connectDragSource?:ConnectDragSource;
	connectDropTarget?:ConnectDropTarget;
	connectDragPreview?:ConnectDragPreview;
}

export interface ModuleState extends BaseState {}


export class ModuleView<P extends ModuleProps = ModuleProps, S extends ModuleState = ModuleState>
	extends React.Component<P, S> {

	protected get children():ReactNode {
		let hasChildren = !!React.Children.count(this.props.children);
		return hasChildren ? this.props.children : null;
	}

}