import classNames from "classnames";
import {
	AlignContentProperty,
	AlignItemsProperty,
	DisplayProperty,
	FlexDirectionProperty,
	FlexWrapProperty,
	JustifyContentProperty,
} from "csstype";
import * as React from "react";

import {sc} from "../../../lib/StyledComponents";
import {DropPlaceholder} from "../../../ui/DropPlaceholder";
import {SectionData} from "../data/SectionData";
import {ModuleProps, ModuleView} from "./ModuleView";

interface Props extends ModuleProps {
	moduleData:SectionData
}

const getStyle = <T1 extends {}>(field) => ((props:Props):T1 => {
	return props.moduleData.config.css[field];
}) as unknown as T1;

@sc`
	flex-direction: ${getStyle<FlexDirectionProperty>('flexDirection')};
	display: ${getStyle<DisplayProperty>('display')};
	flex-wrap: ${getStyle<FlexWrapProperty>('flexWrap')};
	justify-content: ${getStyle<JustifyContentProperty>('justifyContent')};
	align-items: ${getStyle<AlignItemsProperty>('alignItems')};
	align-content: ${getStyle<AlignContentProperty>('alignContent')};
	padding: 20px;
	background-color: transparent;
	
	&.empty {
		padding: 20px;
		background-color: rgba(0,0,0,0.05);
	}
`
export class SectionView extends ModuleView<Props> {

	public render() {

		const {moduleData, className} = this.props;
		const clazzNames = classNames(
			"SectionView",
			className,
			{empty: !moduleData.submodulesIds.length},
		);

		return <div className={clazzNames}>
			{this.children || <DropPlaceholder/>}
		</div>;
	}

}