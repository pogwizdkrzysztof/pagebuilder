import classNames from "classnames";
import * as React from "react";
import {sc} from "../../../lib/StyledComponents";
import {DropPlaceholder} from "../../../ui/DropPlaceholder";
import {MainData} from "../data/MainData";
import {ModuleProps, ModuleView} from "./ModuleView";

interface Props extends ModuleProps {
	moduleData:MainData
}

//@todo @registerModule
@sc`
	&.empty {
		height:100%;
	}
`
export class MainView extends ModuleView<Props> {

	public render() {
		const {moduleData, className} = this.props;
		const clazzNames = classNames(
			"MainView",
			className,
			{empty: !moduleData.submodulesIds.length},
		);

		return <div className={clazzNames}>
			{this.children || <DropPlaceholder/>}
		</div>;
	}

}