import classNames from "classnames";
import {AlignSelfProperty, GlobalsNumber} from "csstype";
import * as React from "react";
import {sc} from "../../../lib/StyledComponents";
import {DropPlaceholder} from "../../../ui/DropPlaceholder";
import {ColumnData} from "../data/ColumnData";
import {ModuleProps, ModuleView} from "./ModuleView";

interface Props extends ModuleProps {
	moduleData:ColumnData
}

const getStyle = <T1 extends {}>(field) => ((props:Props):T1 => {
	return props.moduleData.config.css[field];
}) as unknown as T1;

@sc`
	flex-grow: ${getStyle<GlobalsNumber>('flexGrow')};
	flex-shrink: ${getStyle<GlobalsNumber>('flexShrink')};
	flex-basis: ${getStyle<any>('flexBasis')};
	align-self: ${getStyle<AlignSelfProperty>('alignSelf')};
	padding: 20px;
	background-color: transparent;
	
	&.empty {
		padding: 20px;
		background-color: rgba(0,0,0,0.05);
	}
`
export class ColumnView extends ModuleView<Props> {

	public render() {
		const {moduleData, className} = this.props;
		const clazzNames = classNames(
			"ColumnView",
			className,
			{empty: !moduleData.submodulesIds.length},
		);

		return <div className={clazzNames}>
			{this.children || <DropPlaceholder/>}
		</div>;
	}

}