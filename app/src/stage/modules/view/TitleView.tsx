import classNames from "classnames";
import * as React from "react";
import {sc} from "../../../lib/StyledComponents";
import {TitleData} from "../data/TitleData";
import {ModuleProps, ModuleView} from "./ModuleView";

interface Props extends ModuleProps {
	moduleData:TitleData
}

const cssBoxModel = (props) => {
	return `
		padding: ${props.moduleData.config.css.padding}px;
		margin: ${props.moduleData.config.css.margin}px;
	`;
};

@sc`
	color: ${props => props.moduleData.config.css.color};
	${cssBoxModel}
	&:hover {
		color: blue;
	}
`
export class TitleView extends ModuleView<Props> {

	public render() {
		const {
			moduleData,
			className,
		} = this.props;

		const {
			headingTag,
			headingText,
		} = moduleData.config;

		const props = {
			className: classNames("TitleView", className),
		};

		return <div key={this.props.moduleDataId}>
			{React.createElement(headingTag, props, headingText)}
		</div>;

	}

}