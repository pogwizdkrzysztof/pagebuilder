import * as React from "react";
import {ImageData} from "../data/ImageData";
import {ModuleProps, ModuleView} from "./ModuleView";

interface Props extends ModuleProps {
	moduleData:ImageData
}

export class ImageView extends ModuleView<Props> {

	public render() {
		let {src, alt} = this.props.moduleData.config;
		return <img className={"ImageView"} src={src} alt={alt}/>;
	}

}