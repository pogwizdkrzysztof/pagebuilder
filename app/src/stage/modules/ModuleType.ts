export enum ModuleType {
	MAIN = "main",
	IMAGE = "image",
	TITLE = "title",
	SECTION = "section",
	TITLED_SECTION = "titledSection",
	COLUMN = "column"
}