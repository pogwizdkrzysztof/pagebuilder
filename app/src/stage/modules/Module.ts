import {ModuleData} from "./data/ModuleData";
import {ModuleType} from "./ModuleType";

export interface IModule {
	id: ModuleType;
    name: string;
	icon: string;
	getModuleData(): ModuleData[];
}

export class Module implements IModule {
	public id = null;
	public name = null;
	public icon = null;

	public getModuleData():ModuleData[] {
		return [null];
	}

}