import {ImageData} from "./data/ImageData";
import {IModule} from "./Module";
import {ModuleType} from "./ModuleType";

export class ImageModule implements IModule {
	public id = ModuleType.IMAGE;
	public name = "Image";
	public icon = "image";

	public getModuleData() {
		return [new ImageData()];
	}
}