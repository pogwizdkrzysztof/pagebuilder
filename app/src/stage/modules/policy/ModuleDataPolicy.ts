import {DropAxis} from "../../../dnd/const/DropAxis";
import {ModuleData} from "../data/ModuleData";
import {ModuleType} from "../ModuleType";

export interface IModuleDataPolicy {
	name:string;
	icon?:string;
	/**
	 * null: neither module accepted,
	 * []: all modules accepted,
	 */
	allowedSubmodules?:Array<ModuleType>;
	/**
	 * null: neither module accepted,
	 * []: all modules accepted,
	 */
	disallowedSubmodules?:Array<ModuleType>;
	isDraggable?:boolean;
	isDroppable?:boolean;
	isRemovable?:boolean;
	isDuplicable?:boolean;
	getDropAxis?:(moduleData:ModuleData) => DropAxis
}

export class ModuleDataPolicy implements IModuleDataPolicy {

	public constructor(options:IModuleDataPolicy)
	{
		this.name = options.name;
		this.icon = options.icon || "module";
		this.allowedSubmodules = options.allowedSubmodules || null;
		this.disallowedSubmodules = options.disallowedSubmodules || null;
		this.isDraggable = options.isDraggable !== undefined ? options.isDraggable : true;
		this.isDroppable = options.isDraggable !== undefined ? options.isDroppable : true;
		this.isRemovable = options.isDraggable !== undefined ? options.isRemovable : true;
		this.isDuplicable = options.isDraggable !== undefined ? options.isDuplicable : true;

		this.getDropAxis = options.getDropAxis !== undefined ? options.getDropAxis : (moduleData) => null;
	}

	public name:string;
	public icon:string;

	public allowedSubmodules:Array<ModuleType>;
	public disallowedSubmodules:Array<ModuleType>;

	public isDraggable:boolean;
	public isDroppable:boolean;

	public isRemovable:boolean;
	public isDuplicable:boolean;

	public getDropAxis:(moduleData:ModuleData) => DropAxis;

}