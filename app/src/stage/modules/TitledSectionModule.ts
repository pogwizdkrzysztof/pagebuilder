import {ModuleData} from "./data/ModuleData";
import {TitleData} from "./data/TitleData";
import {TitledSectionData} from "./data/TitledSectionData";
import {IModule} from "./Module";
import {ModuleType} from "./ModuleType";
import {ModulesUtil} from "./utils/ModulesUtil";

export class TitledSectionModule implements IModule {
	public id = ModuleType.TITLED_SECTION;
	public name = "Titled Section";
	public icon = "section";

	public getModuleData():ModuleData[] {

		var titleData = new TitleData(),
			titledSectionData = new TitledSectionData();

		//titleData.id = "title-data-new";
		//titledSectionData.id = "titled-section-data-new";

		ModulesUtil.setParent(titleData, titledSectionData);

		return [
			titledSectionData, // first element is always root
			titleData,
		];

	}
}