import {ModuleType} from "../ModuleType";
import {ModuleData} from "./ModuleData";

export interface ITitledSectionDataConfig {
	columnsQuantity:number,
	columns:string[]
}

export class TitledSectionData extends ModuleData {
	public module = ModuleType.TITLED_SECTION;

	public config:ITitledSectionDataConfig = {
		columnsQuantity: 3,
		columns: [],
	};
}