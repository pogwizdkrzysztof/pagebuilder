import {ModuleType} from "../ModuleType";
import {ModuleData} from "./ModuleData";

export interface ITitleDataConfig {
	headingTag:string;
	headingText:string;
	css:{
		margin:number,
		padding:number
	}
}

export class TitleData extends ModuleData {
	public module = ModuleType.TITLE;

	public config:ITitleDataConfig = {
		headingTag: "h1",
		headingText: "TitleView",
		css: {
			margin: 0,
			padding: 0,
		},
	};
}