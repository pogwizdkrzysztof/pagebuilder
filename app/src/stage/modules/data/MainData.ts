import {ModuleType} from "../ModuleType";
import {ModuleData} from "./ModuleData";

export interface IMainDataConfig {
	css:{
		margin:string,
		maxWidth:number,
	}
}

export class MainData extends ModuleData {
	public module = ModuleType.MAIN;

	public config:IMainDataConfig = {
		css: {
			margin: "40px auto",
			maxWidth: 1200,
		},
	};
}