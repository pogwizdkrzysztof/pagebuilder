import {ModuleType} from "../ModuleType";
import {ModuleData} from "./ModuleData";

export interface ISectionDataConfig {
	columnsQuantity:number,
	columns:string[],
	css:{
		margin:number,
		padding:number,
		flexDirection:string,
		display:string,
		flexWrap:string,
		justifyContent:string,
		alignItems:string,
		alignContent:string
	}
}

export class SectionData extends ModuleData {
	public module = ModuleType.SECTION;

	public config:ISectionDataConfig = {
		columnsQuantity: 3,
		columns: [],
		css:{
			margin: 0,
			padding: 0,
			flexDirection: "row",
			display: "flex",
			flexWrap: "wrap",
			justifyContent: "flex-start",
			alignItems: "flex-start",
			alignContent: "flex-start",
		}
	};
}