import {ModuleType} from "../ModuleType";
import {ModuleData} from "./ModuleData";

export interface IImageDataConfig {
	src:string,
	alt:string
}

export class ImageData extends ModuleData {
	public module = ModuleType.IMAGE;

	public config:IImageDataConfig = {
		src: "http://placehold.it/200x100?text=ImageView",
		alt: "ImageView",
	};
}