import * as uuid from "uuid";
import {ModuleType} from "../ModuleType";

export interface IModuleData {
	id:string;
	name:string;
	icon:string;
	parentId:string;
	module:ModuleType;
	submodulesIds:Array<string>;
	config:any;
}

export class ModuleData implements IModuleData {
	public id = null;
	public name = "";
	public icon = "";
	public parentId = null;
	public module = null;
	public submodulesIds = [];
	public config = {};

	constructor() {
		this.id = uuid.v4();
	}
}