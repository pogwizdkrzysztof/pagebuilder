import {ModuleType} from "../ModuleType";
import {ModuleData} from "./ModuleData";

export interface IColumnDataConfig {
	grid:number,
	css:{
		flexGrow:number,
		flexShrink:number,
		flexBasis:string,
		alignSelf:string
	}
}

export class ColumnData extends ModuleData {
	public module = ModuleType.COLUMN;

	public config:IColumnDataConfig = {
		grid: 12,
		css: {
			flexGrow: 1,
			flexShrink: 0,
			flexBasis: "0px",
			alignSelf: "auto",
		},
	};
}