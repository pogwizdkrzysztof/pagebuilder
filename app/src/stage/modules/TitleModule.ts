import {TitleData} from "./data/TitleData";
import {IModule} from "./Module";
import {ModuleType} from "./ModuleType";

export class TitleModule implements IModule {
	public id = ModuleType.TITLE;
	public name = "Title";
	public icon = "title";

	public getModuleData() {
		return [new TitleData()];
	}
}