import * as React from "react";
import {BaseState} from "../../../App";
import {ImageData} from "../data/ImageData";
import {ModuleConfigProps, ModuleConfigView} from "./ModuleConfigView";

interface Props extends ModuleConfigProps {
	moduleData:ImageData;
}

export class ImageConfigView extends ModuleConfigView<Props, BaseState> {

	public render() {

		const config = this.props.moduleData.config;

		return (
			<div>
				<input
					type="text"
					name="src"
					defaultValue={config.src}
					onChange={this.handleChange()}
				/>
				<input
					type="text"
					name="alt"
					defaultValue={config.alt}
					onChange={this.handleChange()}
				/>
			</div>
		);
	}

}