import * as React from "react";
import {BaseState} from "../../../App";
import {TitleData} from "../data/TitleData";
import {ModuleConfigProps, ModuleConfigView} from "./ModuleConfigView";


interface Props extends ModuleConfigProps {
	moduleData:TitleData;
}

export class TitleConfigView extends ModuleConfigView<Props, BaseState> {

	state = {
		value: 0,
	};

	handleChangeTab = (event, value) => {
		this.setState({ value });
	};

	public render() {

		const config = this.props.moduleData.config;
		const { value } = this.state;

		return (
			<div>

				<input
						id="outlined-email-input"
						type="text"
						name="headingText"
						defaultValue={config.headingText}
						onChange={this.handleChange()}
					/>
				<input
						id="outlined-select-currency"
						name="headingTag"
						value={config.headingTag}
						onChange={this.handleChange(this.config, "headingTag")}
					>
					{/*{['h1','h2','h3','h4','h5','h6'].map((headerType)=>{
							return <MenuItem key={headerType}
											 value={headerType}>
								{headerType}
							</MenuItem>;
						})}*/}
				</input>


				<input
						id="outlined-email-input"
						type="text"
						name="padding"
						defaultValue={config.css.padding}
						onChange={this.handleChange(this.config.css)}
					/>
				<input
						id="outlined-email-input"
						type="text"
						name="margin"
						defaultValue={config.css.margin}
						onChange={this.handleChange(this.config.css)}
					/>


			</div>
		);
	}

}