import * as React from "react";
import {ObjectUtil} from "../../../lib/utils/ObjectUtil";
import {FlexControl} from "../../../ui/moduleConfigurator/form/FlexControl";
import {FlexControlGroup} from "../../../ui/moduleConfigurator/form/FlexControlGroup";
import {ModuleData} from "../data/ModuleData";
import {SectionData} from "../data/SectionData";
import {ModuleType} from "../ModuleType";
import {ModuleConfigProps, ModuleConfigView} from "./ModuleConfigView";

interface Props extends ModuleConfigProps {
	moduleData:SectionData;
}

export class SectionConfigView extends ModuleConfigView<Props> {

	protected handleChangeFlex = (field, value) => {
		this.config.css[field] = value;
		let newModuleData = ObjectUtil.deepClone<ModuleData>(this.props.moduleData);
		newModuleData.config = this.config;
		this.props.onChange(newModuleData);
	};

	public render() {

		const config = this.props.moduleData.config;

		return (
			<div>
				<FlexControlGroup title={"Direction"}
								  property={"flexDirection"}
								  flexbox={config.css}
								  onClick={this.handleChangeFlex}
								  help={""}
				>
					<FlexControl value={"row"} help={"horizontal"}/>
					<FlexControl value={"column"} help={"vertical"}/>
				</FlexControlGroup>

				<FlexControlGroup title={"Wrap"}
								  property={"flexWrap"}
								  flexbox={config.css}
								  onClick={this.handleChangeFlex}
								  help={""}
				>
					<FlexControl value={"nowrap"} help={""}/>
					<FlexControl value={"wrap"} help={""}/>
					<FlexControl value={"wrapReverse"} help={""}/>
				</FlexControlGroup>

				<FlexControlGroup title={"Justify content"}
								  property={"justifyContent"}
								  flexbox={config.css}
								  onClick={this.handleChangeFlex}
								  help={""}
				>
					<FlexControl value={"flex-start"} help={""}/>
					<FlexControl value={"flex-end"} help={""}/>
					<FlexControl value={"center"} help={""}/>
					<FlexControl value={"space-between"} help={""}/>
					<FlexControl value={"space-around"} help={""}/>
				</FlexControlGroup>

				<FlexControlGroup title={"Align items"}
								  property={"alignItems"}
								  flexbox={config.css}
								  onClick={this.handleChangeFlex}
								  help={""}
				>
					<FlexControl value={"flex-start"} help={""}/>
					<FlexControl value={"flex-end"} help={""}/>
					<FlexControl value={"center"} help={""}/>
					<FlexControl value={"baseline"} help={""}/>
					<FlexControl value={"stretch"} help={""}/>
				</FlexControlGroup>

				<FlexControlGroup title={"Align content"}
								  property={"alignContent"}
								  flexbox={config.css}
								  onClick={this.handleChangeFlex}
								  help={""}
				>
					<FlexControl value={"flex-start"} help={""}/>
					<FlexControl value={"flex-end"} help={""}/>
					<FlexControl value={"center"} help={""}/>
					<FlexControl value={"space-between"} help={""}/>
					<FlexControl value={"space-around"} help={""}/>
					<FlexControl value={"stretch"} help={""}/>
				</FlexControlGroup>


				<button onClick={() => this.props.addChild(ModuleType.COLUMN)}>Click to add column</button>
				<button onClick={() => this.props.removeChild()}>Click to remove last column</button>

			</div>
		);


	}

}