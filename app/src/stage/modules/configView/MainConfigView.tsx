import * as React from "react";
import {BaseState} from "../../../App";
import {SectionData} from "../data/SectionData";
import {ModuleConfigProps, ModuleConfigView} from "./ModuleConfigView";

interface Props extends ModuleConfigProps {
	moduleData:SectionData;
}

export class MainConfigView extends ModuleConfigView<Props, BaseState> {

	public render() {
		const config = this.props.moduleData.config;
		return (
			<div></div>
		);
	}

}