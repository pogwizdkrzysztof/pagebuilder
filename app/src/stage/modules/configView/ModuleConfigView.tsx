import * as React from "react";
import {BaseProps, BaseState} from "../../../App";
import {ObjectUtil} from "../../../lib/utils/ObjectUtil";
import {ModuleData} from "../data/ModuleData";
import {ModuleType} from "../ModuleType";

export interface ModuleConfigProps extends BaseProps {
	moduleData:any;
	onChange?:(moduleData:ModuleData) => void,
	addChild?:(moduleType:ModuleType) => void,
	removeChild?:() => void,
}

export interface ModuleConfigState extends BaseState {
}

export class ModuleConfigView<P extends ModuleConfigProps = ModuleConfigProps, S extends ModuleConfigState = ModuleConfigState>
	extends React.PureComponent<ModuleConfigProps, ModuleConfigState> {

	protected config;

	constructor(props, context) {
		super(props,context);
		this.config = ObjectUtil.deepClone(props.moduleData).config;
	}

	protected handleChange = (reference = this.config, prop?:string) => {
		return (event) => {
			prop = prop || event.target.getAttribute('name');
			reference[prop]	= event.target.value;
			let newModuleData = ObjectUtil.deepClone<ModuleData>(this.props.moduleData);
			newModuleData.config = this.config;
			this.props.onChange(newModuleData);
		}
	}

}