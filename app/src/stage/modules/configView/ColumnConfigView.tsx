import * as React from "react";
import {BaseState} from "../../../App";
import {ColumnData} from "../data/ColumnData";
import {ModuleConfigProps, ModuleConfigView} from "./ModuleConfigView";

interface Props extends ModuleConfigProps {
	moduleData:ColumnData;
}

export class ColumnConfigView extends ModuleConfigView<Props, BaseState> {

	public render() {

		const config = this.props.moduleData.config;

		return (
			<div>
				<input
					type="text"
					name="flexGrow"
					defaultValue={config.css.flexGrow}
					onChange={this.handleChange(this.config.css)}
				/>
				<input
					type="text"
					name="flexShrink"
					defaultValue={config.css.flexShrink}
					onChange={this.handleChange(this.config.css)}
				/>
				<input
					type="text"
					name="flexBasis"
					defaultValue={config.css.flexBasis}
					onChange={this.handleChange(this.config.css)}
				/>
				<input
					type="text"
					name="alignSelf"
					defaultValue={config.css.alignSelf}
					onChange={this.handleChange(this.config.css)}
				/>
			</div>
		);
	}

}