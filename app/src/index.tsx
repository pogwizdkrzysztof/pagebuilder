import * as React from "react";
import {DndProvider} from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import * as ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {App} from "./App";
import {AppInitializer} from "./initializers/AppInitializer";
import {M} from "./models/M";

(async () => {
	const appElement = document.getElementById('app');
	if (!appElement)
		throw new Error('App container does not exist');

	AppInitializer.init();

	const app = (
		<Provider store={M.store.store}>
			<DndProvider backend={HTML5Backend}>
				<App/>
			</DndProvider>
		</Provider>
	);
	ReactDOM.render(app, appElement);

})();