import * as React from "react";
import {DndProvider} from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import ReactDOMServer from "react-dom/server";
import {Provider} from "react-redux";
import {M} from "../models/M";
import {Stage} from "../stage/Stage";

export class MarkupRenderManager {

	public static getMarkup():string {
		const appMarkup = ReactDOMServer.renderToString(
			<Provider store={M.store.store}>
				<DndProvider backend={HTML5Backend}>
					<Stage/>
				</DndProvider>
			</Provider>,
		);

		const websiteMarkup = ReactDOMServer.renderToString(
			<html>
			<head>
				<style type="text/css">{}</style>
			</head>
			<body dangerouslySetInnerHTML={{__html: appMarkup}}></body>
			</html>,
		);

		return websiteMarkup;
	}

}