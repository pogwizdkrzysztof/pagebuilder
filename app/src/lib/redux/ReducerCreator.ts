import {Action} from "redux";

export class ReducerCreator<S, A extends Action<any> = any> {
	public initialState:S;
	public spec:{ [key:string]:((state:S, action:A) => S) };
}