import {connect} from "react-redux";
import {Action, Reducer} from "redux";
import {M} from "../../models/M";
import {PromiseUtil} from "../utils/PromiseUtil";
import {ReducerCreator} from "./ReducerCreator";

export function createReducer<S, A extends Action<any> = any>(reducer:ReducerCreator<S, A>):Reducer<S, A> {
	return (state:S = reducer.initialState, action:A):S => {
		for (let key in reducer.spec) {
			if (action.type === key) {
				return reducer.spec[key](state, action);
			}
		}
		return state;
	};
}

export function dispatchable(target:any, propertyKey:string, desc:PropertyDescriptor) {
	const originalAction:Function = desc.value;
	desc.value = function (...args) {

		const result = originalAction.apply(null, args);

		if (!PromiseUtil.isPromise(result)) {
			return M.store.dispatch(result);
		}

		return new Promise((resolve, reject) => {
			(result as Promise<any>)
				.then((promiseResult) => {
					resolve(M.store.dispatch(promiseResult));
				})
				.catch((error) => {
					console.log(`dispatchable error, ${originalAction.name}`, error);
					reject(error);
				});
		});

	};
	return desc;
}

export function combine(mapStateToProps) {
	return function (constructor) {
		return connect(mapStateToProps)(constructor) as any;
	};
}