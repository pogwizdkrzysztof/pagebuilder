import styled from 'styled-components';

export function sc(strings, ...values) {
	return function (constructor) {
		const decoratedConstructor = styled(constructor)(strings, ...values);
		return decoratedConstructor;
	};
}