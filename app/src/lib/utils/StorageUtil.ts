export enum StorageName {
	SESSION_STORAGE = "SESSION_STORAGE",
	LOCAL_STORAGE = "LOCAL_STORAGE",
}

export class StorageUtil {

	protected static isAvailable(storageName:StorageName = StorageName.SESSION_STORAGE):boolean {
		try {
			if (!window[storageName])
				return false;
			let key = `storage-test-${Date.now()}`;
			window[storageName].setItem(key, "");
			window[storageName].removeItem(key);
			return true;
		} catch (exception) {
			return false;
		}
	}

	public static loadState() {
		try {
			const serializedState = localStorage.getItem('state');
			if (serializedState === null) {
				return undefined;
			}
			let parsedState = JSON.parse(serializedState);

			return parsedState;
		} catch (error) {
			console.log('cannot load state from local storage');
			return undefined;
		}
	}

	public static saveState(state) {
		try {
			const serializedState = JSON.stringify(state);
			localStorage.setItem('state', serializedState);
		} catch (error) {
			console.log('cannot save state to local storage');
		}
	}

}