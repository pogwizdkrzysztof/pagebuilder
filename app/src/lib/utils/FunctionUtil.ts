export class FunctionUtil {

	public static sleep(timeMs:number):Promise<any> {
		return new Promise<any>((resolve, reject) => {
			setTimeout(resolve, timeMs);
		}) as any;
	}

}