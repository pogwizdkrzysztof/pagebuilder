const clone = require('clone');

export class ObjectUtil {

	public static clone<T = Object>(object:T):T {
		return Object.assign(Object.create(Object.getPrototypeOf(object)), object);
	}

	public static deepClone<T = Object>(object:T):T {
		return clone(object);
	}

}