export class ScrollPosition {
	public scrollTop:number;
	public scrollLeft:number;

	constructor(scrollTop:number = 0, scrollLeft:number = 0) {
		this.scrollTop = scrollTop;
		this.scrollLeft = scrollLeft;
	}
}