import {Action} from "redux";
import {dispatchable} from "../lib/redux/ReduxDecorators";
import {AppUISelectors} from "../selectors/AppUISelectors";
import {ToolbarData} from "../stageUI/ToolbarData";
import {ToolbarsActionsTypes} from "./types/ToolbarsActionsTypes";

export interface ToolbarsAction extends Action<ToolbarsActionsTypes> {
	type:ToolbarsActionsTypes,
	moduleToolbar?:ToolbarData
	moduleId?:string
}

export class ToolbarsActions {

	@dispatchable
	public static add(moduleId:string, ref:HTMLElement) { // @todo: improve type
		let rect = ref.getBoundingClientRect();
		let scrollPos = AppUISelectors.getScrollPosition();
		const scrollLeft = scrollPos.scrollLeft;
		const scrollTop = scrollPos.scrollTop;
		let moduleRect = {
			top: rect.top + scrollTop,
			left: rect.left + scrollLeft,
			width: rect.width,
			height: rect.height,
		} as ClientRect;

		let toolbar = new ToolbarData(
			moduleId,
			moduleRect,
		);

		return {
			type: ToolbarsActionsTypes.ADD_MODULE_TOOLBAR,
			moduleToolbar: toolbar,
		};
	}

	@dispatchable
	public static remove(moduleId:string) { // @todo: improve type
		return {
			type: ToolbarsActionsTypes.REMOVE_MODULE_TOOLBAR,
			moduleId: moduleId,
		};
	}

	@dispatchable
	public static clear() {
		return {
			type: ToolbarsActionsTypes.CLEAR_MODULES_TOOLBARS,
		};
	}
}