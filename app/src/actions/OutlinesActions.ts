import {Action} from "redux";
import {dispatchable} from "../lib/redux/ReduxDecorators";
import {OutlinesActionsTypes} from "./types/OutlinesActionsTypes";

export interface OutlinesAction extends Action<OutlinesActionsTypes> {
	type:OutlinesActionsTypes,
	moduleId:string
	rect:ClientRect
}

export class OutlinesActions {

	@dispatchable
	public static add(moduleId:string, rect:ClientRect = null) {
		return {
			type: OutlinesActionsTypes.ADD_MODULE_OUTLINE,
			moduleId: moduleId,
			rect: rect,
		};
	}

	@dispatchable
	public static remove(moduleId:string) {
		return {
			type: OutlinesActionsTypes.REMOVE_MODULE_OUTLINE,
			moduleId: moduleId,
		};
	}

	@dispatchable
	public static clear() {
		return {
			type: OutlinesActionsTypes.CLEAR_MODULES_OUTLINE,
		};
	}
}