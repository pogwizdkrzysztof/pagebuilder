import keyBy from "lodash/keyBy";
import mapValues from "lodash/mapValues";
import {Action} from "redux";
import {MoveDirection} from "../const/MoveDirection";
import {dispatchable} from "../lib/redux/ReduxDecorators";
import {StageSelectors} from "../selectors/StageSelectors";
import {ModuleData} from "../stage/modules/data/ModuleData";
import {IModule} from "../stage/modules/Module";
import {StageActionsTypes} from "./types/StageActionsTypes";

export interface StageAction extends Action<StageActionsTypes> {
	type:StageActionsTypes,
	modulesDatas?:ModuleData[],
	allIds?:string[],
	byId?:{ [key:string]:ModuleData },
	moveDirection?:MoveDirection,
	parentModule?:ModuleData,
	submoduleId?:string
	moduleDataId?:string
}

export class StageActions {

	@dispatchable
	public static addModules(module:IModule):StageAction {
		const modules = module.getModuleData();
		const parentModule = StageSelectors.getMainModuleData();
		return {
			type: StageActionsTypes.ADD_STAGE_MODULE,
			modulesDatas: modules,
			parentModule: parentModule,
		};
	};

	@dispatchable
	public static selectModule(moduleDataId:string):StageAction {

		if (StageSelectors.isInConfigMode()) {

		}

		return {
			type: StageActionsTypes.SELECT_STAGE_MODULE,
			moduleDataId: moduleDataId,
		};
	};

	@dispatchable
	public static deselectModule():StageAction {
		return {
			type: StageActionsTypes.DESELECT_STAGE_MODULE,
		};
	};

	@dispatchable
	public static toggleEditModule(moduleDataId:string):StageAction {

		let moduleData = StageSelectors.getModuleData(moduleDataId);
		let allIds = [moduleData.id, ...moduleData.submodulesIds];
		let modulesDatas = StageSelectors.getModulesDatas(allIds);
		//@todo: create smth like .toMap(), .toArray() method in ModulesUtil
		let byId = {
			...mapValues(keyBy(modulesDatas, 'id')),
		};

		return {
			type: StageActionsTypes.EDIT_STAGE_MODULE,
			allIds: allIds,
			byId: byId,
		};
	};

	@dispatchable
	public static cloneModules(modulesDatas:ModuleData[]):StageAction {
		return {
			type: StageActionsTypes.CLONE_STAGE_MODULE,
			modulesDatas: modulesDatas,
		};
	};

	@dispatchable
	public static removeModules(modulesDatas:ModuleData[]):StageAction {
		return {
			type: StageActionsTypes.REMOVE_STAGE_MODULE,
			modulesDatas: modulesDatas,
		};
	};

	// @dispatchable
	// public static saveModules(modulesDatas:ModuleData[]):StageAction {
	// 	return {
	// 		type: StageActionsTypes.SAVE_STAGE_MODULE,
	// 		modulesDatas: modulesDatas, // unused?
	// 	};
	// };

	@dispatchable
	public static restoreModules(modulesDatas:ModuleData[]):StageAction {
		return {
			type: StageActionsTypes.RESTORE_STAGE_MODULE,
			modulesDatas: modulesDatas,
		};
	};

	@dispatchable
	public static forceUpdateModules(modulesDatas:ModuleData[]):StageAction {
		return {
			type: StageActionsTypes.FORCE_UPDATE_STAGE_MODULE,
			modulesDatas: modulesDatas,
		};
	};

	@dispatchable
	public static updateModules(modulesDatas:ModuleData[]):StageAction {
		return {
			type: StageActionsTypes.UPDATE_STAGE_MODULE,
			modulesDatas: modulesDatas,
		};
	};

	@dispatchable
	public static moveModules(modulesDatas:ModuleData[], moveDirection:MoveDirection):StageAction {
		return {
			type: StageActionsTypes.MOVE_STAGE_MODULE,
			modulesDatas: modulesDatas,
			moveDirection: moveDirection,
		};
	};

	@dispatchable
	public static saveModules():StageActions {
		let modules = StageSelectors.getTemporaryModules();
		return {
			type: StageActionsTypes.SAVE_STAGE_MODULE,
			modulesDatas: modules,
		};
	};

	@dispatchable
	public static addChildModule(modulesDatas:ModuleData[]):StageActions {
		return {
			type: StageActionsTypes.ADD_CHILD_STAGE_MODULE,
			modulesDatas: modulesDatas,
		};
	};

	@dispatchable
	public static removeChildModule(modulesDatas:ModuleData[], submoduleId:string):StageActions {
		return {
			type: StageActionsTypes.REMOVE_CHILD_STAGE_MODULE,
			modulesDatas: modulesDatas,
			submoduleId: submoduleId,
		};
	};
}