import {Action} from "redux";
import {dispatchable} from "../lib/redux/ReduxDecorators";
import {DebugActionsTypes} from "./types/DebugActionsTypes";

export interface DebugAction extends Action<DebugActionsTypes> {
	type:DebugActionsTypes
}

export class DebugActions {

	@dispatchable
	public static toggleIds():DebugAction {
		return {
			type: DebugActionsTypes.TOGGLE_IDS,
		};
	}

	@dispatchable
	public static toggleBorders():DebugAction {
		return {
			type: DebugActionsTypes.TOGGLE_BORDERS,
		};
	}

	@dispatchable
	public static saveState():DebugAction {
		return {
			type: DebugActionsTypes.SAVE_STATE
		}
	}

	@dispatchable
	public static loadState():DebugAction {
		return {
			type: DebugActionsTypes.LOAD_STATE
		}
	}
}