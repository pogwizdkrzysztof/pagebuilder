import {Action} from "redux";
import {DragAndDropItemTypes} from "../dnd/const/DragAndDropItemTypes";
import {DropSide} from "../dnd/const/DropSide";
import {DnDUtil} from "../dnd/utils/DnDUtil";
import {dispatchable} from "../lib/redux/ReduxDecorators";
import {ObjectUtil} from "../lib/utils/ObjectUtil";
import {M} from "../models/M";
import {ModulesModel} from "../models/ModulesModel";
import {ModuleData} from "../stage/modules/data/ModuleData";
import {Module} from "../stage/modules/Module";
import {ModuleType} from "../stage/modules/ModuleType";
import {DragAndDropActionsTypes} from "./types/DragAndDropActionsTypes";

export interface DragAndDropAction extends Action<DragAndDropActionsTypes> {
	type:DragAndDropActionsTypes,
	dragModuleId?:string,
	dropOverModuleId?:string,
	dropModuleId?:string,
	dropOverModuleRect?:ClientRect,
	dropSide?:DropSide,
	modulesDatas?:ModuleData[],
	modulesToUpdate?:Array<{ [key:string]:ModuleData }>
}

export class DragAndDropActions {

	@dispatchable
	public static dropStageModule(
		itemType:DragAndDropItemTypes,
		sourceModuleId:string,
		moduleType:ModuleType,
		dropSide:DropSide,
		targetOverModuleId:string,
		targetModuleId:string,
	)
	{

		// #1. przygotować element do wrzucenia w nowe miejsce
		// #2. jeśli przenoszony - usunąć z poprzedniego parenta
		// #3. jeśli przenoszony - dodać do nowego parenta
		// #4. jeśli dodany - dodać do `allIds`

		const state = M.store.getState();
		const stageModulesById = state.stage.present.byId;

		let dragModuleData:ModuleData[] = null; // as array, because of nested ModuleDatas (e.g.
												// TitledSectionModule)
		let dropModuleData:ModuleData = null;
		let modulesToUpdate:Array<{ [key:string]:ModuleData }> = [];

		const isNewModule = itemType == DragAndDropItemTypes.PLAIN_MODULE; // if dropped from ModulesList
		const isDroppedIntoModule = !!targetModuleId;

		if (isDroppedIntoModule)
			dropModuleData = ObjectUtil.deepClone(stageModulesById[targetModuleId]);

		// #1.
		if (isNewModule) {
			let module:Module = ModulesModel.getModule(moduleType);
			dragModuleData = module.getModuleData();
		}
		else {
			dragModuleData = [ObjectUtil.deepClone(stageModulesById[sourceModuleId])];
		}

		let mainDragModuleData = dragModuleData[0];
		let prevParentModuleData:ModuleData = null;
		let nextParentModuleData:ModuleData = null;

		// #2.
		// Update previously containing droppedModuleData (to update view) if there was any
		if (!isNewModule) {
			if (mainDragModuleData.parentId) {
				prevParentModuleData = ObjectUtil.deepClone(stageModulesById[mainDragModuleData.parentId]);
				let index = prevParentModuleData.submodulesIds.indexOf(mainDragModuleData.id);
				prevParentModuleData.submodulesIds.splice(index, 1);
			}
		}

		// If moduleData inserted (moved) into other (or the same parent) moduleData, update parentId
		if (isDroppedIntoModule) {
			mainDragModuleData.parentId = dropModuleData.id;
		}
		else {
			mainDragModuleData.parentId = null;
			throw new Error('parentId is null. Probably there is no dndData in state.');
		}

		// #3.
		// Update next containing droppedModuleData (to display in view)
		if (isDroppedIntoModule) {
			nextParentModuleData = ObjectUtil.deepClone(stageModulesById[dropModuleData.id]);

			// Update local drop index
			let prevDragModuleDataIndex = nextParentModuleData.submodulesIds.indexOf(sourceModuleId);
			let draggedWithinSameModule = prevDragModuleDataIndex !== -1;

			// jesli przeniesiony w obrebie tego samego modulu, to usuwamy
			if (draggedWithinSameModule)
				nextParentModuleData.submodulesIds.splice(prevDragModuleDataIndex, 1);

			let nextDragModuleDataIndex = nextParentModuleData.submodulesIds.indexOf(targetOverModuleId);
			let isDroppedOnDraggedOverModule = targetOverModuleId == targetModuleId;

			// dropujemy na elemencie który ma przyjąć element jako dziecko
			// to obliczamy czy wrzucicć na koniec czy na poczatek
			if (isDroppedOnDraggedOverModule) {
				nextDragModuleDataIndex = (dropSide === DropSide.AFTER || dropSide === DropSide.BELOW) ?
					nextParentModuleData.submodulesIds.length : 0;
			}

			let newDragModuleIndex = DnDUtil.getDropIndex(nextDragModuleDataIndex, dropSide);
			nextParentModuleData.submodulesIds.splice(newDragModuleIndex, 0, mainDragModuleData.id);
		}

		// add all ModuleDatas (except main which is first)
		for (let i = 1; i < dragModuleData.length; i++)
			modulesToUpdate.push({[dragModuleData[i].id]: dragModuleData[i]});

		// add prev parent if there was
		if (!!prevParentModuleData)
			modulesToUpdate.push({[prevParentModuleData.id]: prevParentModuleData});

		// add next parent if there is
		if (!!nextParentModuleData)
			modulesToUpdate.push({[nextParentModuleData.id]: nextParentModuleData});

		return {
			type: DragAndDropActionsTypes.DROP_STAGE_MODULE,
			modulesDatas: [mainDragModuleData],
			modulesToUpdate: modulesToUpdate,
		};

	}

	@dispatchable
	public static updateDropInfo(
		dropSide:DropSide = null,
		targetOverModuleRect:ClientRect = null,
		targetOverModuleId:string = null,
		targetModuleId:string = null,
		dragModuleId:string = null):DragAndDropAction
	{
		return {
			type: DragAndDropActionsTypes.UPDATE_DROP_INFO,
			dropSide: dropSide,
			dropOverModuleRect: targetOverModuleRect,
			dropOverModuleId: targetOverModuleId,
			dropModuleId: targetModuleId,
			dragModuleId: dragModuleId,
		};
	}

	@dispatchable
	public static beginDrag():DragAndDropAction
	{
		return {
			type: DragAndDropActionsTypes.BEGIN_DRAG,
		};
	}

	@dispatchable
	public static endDrag():DragAndDropAction
	{
		return {
			type: DragAndDropActionsTypes.END_DRAG,
		};
	}
}