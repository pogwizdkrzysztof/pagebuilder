export enum AppUIActionsTypes {
	TOGGLE_TAB = "TOGGLE_TAB",
	SAVE_SCROLL = "SAVE_SCROLL"
}