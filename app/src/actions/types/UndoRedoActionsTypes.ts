export enum UndoRedoActionsTypes {
	UNDO = "UNDO",
	REDO = "REDO"
}