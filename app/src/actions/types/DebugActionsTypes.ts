export enum DebugActionsTypes {
	TOGGLE_IDS = "TOGGLE_IDS",
	TOGGLE_BORDERS = "TOGGLE_BORDERS",
	SAVE_STATE = "SAVE_STATE",
	LOAD_STATE = "LOAD_STATE"
}