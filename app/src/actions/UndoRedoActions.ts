import {Action} from "redux";
import {dispatchable} from "../lib/redux/ReduxDecorators";
import {UndoRedoActionsTypes} from "./types/UndoRedoActionsTypes";

export interface UndoRedoAction extends Action<UndoRedoActionsTypes> {
	type:UndoRedoActionsTypes
}

export class UndoRedoActions {

	@dispatchable
	public static undo():UndoRedoAction {
		return {
			type: UndoRedoActionsTypes.UNDO,
		};
	}

	@dispatchable
	public static redo():UndoRedoAction {
		return {
			type: UndoRedoActionsTypes.REDO,
		};
	}
}

