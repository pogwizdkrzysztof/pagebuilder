import {Action} from "redux";
import {AppUITab} from "../const/AppUITab";
import {dispatchable} from "../lib/redux/ReduxDecorators";
import {ScrollPosition} from "../lib/ScrollPosition";
import {AppUIActionsTypes} from "./types/AppUIActionsTypes";

export interface AppUIAction extends Action<AppUIActionsTypes> {
	type:AppUIActionsTypes,
	tab?:AppUITab
	scrollPosition?:ScrollPosition
}

export class AppUIActions {

	@dispatchable
	public static toggleTab(tab:AppUITab):AppUIAction {
		return {
			type: AppUIActionsTypes.TOGGLE_TAB,
			tab: tab,
		};
	}

	@dispatchable
	public static saveScroll(scrollPosition:ScrollPosition):AppUIAction {
		return {
			type: AppUIActionsTypes.SAVE_SCROLL,
			scrollPosition: scrollPosition,
		};
	}
}