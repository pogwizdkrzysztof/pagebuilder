import {Action} from "redux";
import {AppStatus} from "../const/AppStatus";
import {dispatchable} from "../lib/redux/ReduxDecorators";
import {AppActionsTypes} from "./types/AppActionsTypes";

export interface AppAction extends Action<AppActionsTypes> {
	type:AppActionsTypes,
	status:AppStatus
}

export class AppActions {

	@dispatchable
	public static load(status:AppStatus):AppAction {
		return {
			type: AppActionsTypes.LOAD_APP,
			status: status,
		};
	}

}