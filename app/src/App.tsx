import * as React from "react";
import {Scrollbars} from 'react-custom-scrollbars';
import {connect, MapStateToProps} from "react-redux";
import {AppActions} from "./actions/AppActions";
import {AppUIActions} from "./actions/AppUIActions";
import {AppStatus} from "./const/AppStatus";
import {ScrollPosition} from "./lib/ScrollPosition";
import {RootState} from "./reducers/RootReducer";
import {AppSelectors} from "./selectors/AppSelectors";
import {Stage} from "./stage/Stage";
import {AppError} from "./ui/AppError";
import {AppLoader} from "./ui/AppLoader";
import {DnDPlaceholder} from "./ui/DnDPlaceholder";
import {ModuleDragLayerView} from "./ui/ModuleDragLayerView";
import {Outlines} from "./ui/moduleToolbar/Outlines";
import {Toolbars} from "./ui/moduleToolbar/Toolbars";
import {UISidebar} from "./ui/sidebar/UISidebar";

export interface BaseProps {
	key?:string | number;
	className?:string;
}

export interface BaseState {}

interface Props extends BaseProps, Partial<StateProps> {}

interface StateProps {
	status:AppStatus
}

interface State extends BaseState {
	hasError:boolean
}

const mapStateToProps:MapStateToProps<StateProps, Props, RootState> = (state, ownProps):StateProps => {
	return {
		status: AppSelectors.getStatus(state),
	};
};

// @(DragDropContext(HTML5Backend) as any)
@(connect<StateProps, {}, Props, BaseState>(mapStateToProps) as any)
export class App extends React.Component<Props, State> {

	private scrollPosition = new ScrollPosition();

	constructor(props) {
		super(props);
		this.state = {
			hasError: false,
		};
	}

	public componentDidMount():void {
		AppActions.load(AppStatus.SUCCESS);
	}

	public componentDidCatch(error, errorInfo) {
		console.log(error, errorInfo);
		this.setState({hasError: true});
	}

	public render() {
		const {status} = this.props;

		if (status === AppStatus.REQUEST) {
			return <div className="App">
				<AppLoader/>
			</div>;
		}

		if (status === AppStatus.FAILURE || this.state.hasError) {
			return <div className="App">
				<AppError/>
			</div>;
		}

		return (
			<div className="App">
				<div className={"AppUI"}>
					<Scrollbars className={"StageContainer"}
								onScrollFrame={(values) => {
									this.scrollPosition = new ScrollPosition(values.scrollTop, values.scrollLeft);
								}}
								onScrollStop={() => AppUIActions.saveScroll(this.scrollPosition)}
					>
						<Stage/>
						<div style={{position: "absolute", left: 0, top: 0}}>
							<Outlines/>
							<Toolbars/>
							<DnDPlaceholder/>
							<ModuleDragLayerView/>
						</div>
					</Scrollbars>
					<UISidebar/>
				</div>
			</div>
		);
	}
}