import {DropAxis} from "../dnd/const/DropAxis";
import {ColumnModule} from "../stage/modules/ColumnModule";
import {ColumnConfigView} from "../stage/modules/configView/ColumnConfigView";
import {ImageConfigView} from "../stage/modules/configView/ImageConfigView";
import {MainConfigView} from "../stage/modules/configView/MainConfigView";
import {SectionConfigView} from "../stage/modules/configView/SectionConfigView";
import {TitleConfigView} from "../stage/modules/configView/TitleConfigView";
import {TitledSectionConfigView} from "../stage/modules/configView/TitledSectionConfigView";
import {ColumnData} from "../stage/modules/data/ColumnData";
import {ImageData} from "../stage/modules/data/ImageData";
import {MainData} from "../stage/modules/data/MainData";
import {SectionData} from "../stage/modules/data/SectionData";
import {TitleData} from "../stage/modules/data/TitleData";
import {TitledSectionData} from "../stage/modules/data/TitledSectionData";
import {ImageModule} from "../stage/modules/ImageModule";
import {MainModule} from "../stage/modules/MainModule";
import {Module} from "../stage/modules/Module";
import {ModuleType} from "../stage/modules/ModuleType";
import {ModuleDataPolicy} from "../stage/modules/policy/ModuleDataPolicy";
import {SectionModule} from "../stage/modules/SectionModule";
import {TitledSectionModule} from "../stage/modules/TitledSectionModule";
import {TitleModule} from "../stage/modules/TitleModule";
import {ColumnView} from "../stage/modules/view/ColumnView";
import {ImageView} from "../stage/modules/view/ImageView";
import {MainView} from "../stage/modules/view/MainView";
import {SectionView} from "../stage/modules/view/SectionView";
import {TitledSectionView} from "../stage/modules/view/TitledSectionView";
import {TitleView} from "../stage/modules/view/TitleView";

export class ModulesModel {

	public static modules = {
		[ModuleType.IMAGE]: ImageModule,
		[ModuleType.TITLE]: TitleModule,
		[ModuleType.SECTION]: SectionModule,
		[ModuleType.TITLED_SECTION]: TitledSectionModule,
		[ModuleType.COLUMN]: ColumnModule,
		[ModuleType.MAIN]: MainModule,
	};

	public static datas = {
		[ModuleType.IMAGE]: ImageData,
		[ModuleType.TITLE]: TitleData,
		[ModuleType.SECTION]: SectionData,
		[ModuleType.TITLED_SECTION]: TitledSectionData,
		[ModuleType.COLUMN]: ColumnData,
		[ModuleType.MAIN]: MainData,
	};

	public static configs = {
		[ModuleType.IMAGE]: ImageConfigView,
		[ModuleType.TITLE]: TitleConfigView,
		[ModuleType.SECTION]: SectionConfigView,
		[ModuleType.TITLED_SECTION]: TitledSectionConfigView,
		[ModuleType.COLUMN]: ColumnConfigView,
		[ModuleType.MAIN]: MainConfigView,
	};

	public static views = {
		[ModuleType.IMAGE]: ImageView,
		[ModuleType.TITLE]: TitleView,
		[ModuleType.SECTION]: SectionView,
		[ModuleType.TITLED_SECTION]: TitledSectionView,
		[ModuleType.COLUMN]: ColumnView,
		[ModuleType.MAIN]: MainView,
	};

	public static policies = {
		[ModuleType.IMAGE]: new ModuleDataPolicy({name: "Image", icon: "image"}),
		[ModuleType.TITLE]: new ModuleDataPolicy({name: "Title", icon: "title"}),
		[ModuleType.SECTION]: new ModuleDataPolicy({
			name: "Section",
			icon: "section",
			allowedSubmodules: [ModuleType.COLUMN],
			getDropAxis: (moduleData:SectionData) => {
				let isRow = moduleData.config.css.flexDirection === "row";
				return isRow ?
					DropAxis.X :
					DropAxis.Y;
			},
		}),
		[ModuleType.TITLED_SECTION]: new ModuleDataPolicy({
			name: "Titled section",
			icon: "titled-section",
			allowedSubmodules: [],
			disallowedSubmodules: [ModuleType.SECTION],
		}),
		[ModuleType.COLUMN]: new ModuleDataPolicy({
			name: "Column",
			icon: "column",
			allowedSubmodules: [],
			disallowedSubmodules: [ModuleType.COLUMN],
		}),
		[ModuleType.MAIN]: new ModuleDataPolicy({
			name: "Main",
			icon: "main",
			allowedSubmodules: [ModuleType.SECTION],
			isDraggable: false,
			isRemovable: false,
			isDuplicable: false,
		}),
	};

	public static getModule(moduleType:ModuleType):Module {
		const clazz = ModulesModel.modules[moduleType];
		if (!!clazz)
			return new clazz();
		else
			throw new Error('There is no ModuleType: ' + moduleType);
	}

}