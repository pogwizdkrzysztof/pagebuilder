import {Action, applyMiddleware, createStore, Store} from "redux";
import {createLogger} from "redux-logger";
import thunk from "redux-thunk";
import {AppAction} from "../actions/AppActions";
import {AppUIAction} from "../actions/AppUIActions";
import {DebugAction} from "../actions/DebugActions";
import {DragAndDropAction} from "../actions/DragAndDropActions";
import {StageAction} from "../actions/StageActions";
import {ToolbarsAction} from "../actions/ToolbarsActions";
import {OutlinesActionsTypes} from "../actions/types/OutlinesActionsTypes";
import {ToolbarsActionsTypes} from "../actions/types/ToolbarsActionsTypes";
import {UndoRedoAction} from "../actions/UndoRedoActions";
import {RootReducer, RootState} from "../reducers/RootReducer";
import {M} from "./M";

type StoreAction =
	AppAction &
	DebugAction &
	DragAndDropAction &
	ToolbarsAction &
	StageAction &
	AppUIAction &
	UndoRedoAction;

export class StoreModel {

	public store:Store<RootState> = null;

	constructor() {

		let devMiddlewares = [];

		const actionsToIgnore = [
			ToolbarsActionsTypes.ADD_MODULE_TOOLBAR,
			ToolbarsActionsTypes.REMOVE_MODULE_TOOLBAR,
			ToolbarsActionsTypes.CLEAR_MODULES_TOOLBARS,
			// OutlinesActionsTypes.ADD_MODULE_OUTLINE,
			OutlinesActionsTypes.REMOVE_MODULE_OUTLINE,
			// OutlinesActionsTypes.CLEAR_MODULES_OUTLINE,
			// DragAndDropActionsTypes.UPDATE_DROP_INFO,
		];

		if (M.env.isDev) {
			// const freeze = require('redux-freeze');
			const logger = createLogger({
				predicate: (getState:Function, action:Action) => {
					return actionsToIgnore.indexOf(action.type) === -1;
				},
				collapsed: true,
			});

			devMiddlewares = [
				// freeze,
				logger,
			];

		}

		this.store = createStore<RootState, StoreAction, any, any>(RootReducer,
			applyMiddleware(thunk, ...devMiddlewares));
	}

	public getState():RootState {
		return this.store.getState();
	}

	public dispatch(action:StoreAction):void {
		this.store.dispatch(action);
	}

}