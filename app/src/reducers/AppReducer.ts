import {AppAction} from "../actions/AppActions";
import {AppActionsTypes} from "../actions/types/AppActionsTypes";
import {AppStatus} from "../const/AppStatus";
import {ReducerCreator} from "../lib/redux/ReducerCreator";
import {AppState} from "./states/AppState";

export class AppReducer extends ReducerCreator<AppState, AppAction> {

	public initialState = {
		status: AppStatus.FAILURE,
	};

	public spec = {
		[AppActionsTypes.LOAD_APP]: this.loadApp,
	};

	public loadApp(state:AppState, action:AppAction) {
		return {
			status: action.status,
		};
	}

}