import {Reducer} from "redux";
import {AppUIAction} from "../actions/AppUIActions";
import {AppUIActionsTypes} from "../actions/types/AppUIActionsTypes";
import {AppUITab} from "../const/AppUITab";
import {ScrollPosition} from "../lib/ScrollPosition";

export interface AppUIState {
	activeTab:AppUITab,
	scrollPosition:ScrollPosition
}

const initialState:AppUIState = {
	activeTab: null,
	scrollPosition: new ScrollPosition(),
};

export const appUI:Reducer<AppUIState, AppUIAction> = (state = initialState, action) => {
	switch (action.type) {
		case AppUIActionsTypes.TOGGLE_TAB:
			let tab = action.tab === state.activeTab ? initialState.activeTab : action.tab;
			return {
				...state,
				activeTab: tab,
			};
		case AppUIActionsTypes.SAVE_SCROLL:
			return {
				...state,
				scrollPosition: action.scrollPosition,
			};
		default:
			return state;
	}
};