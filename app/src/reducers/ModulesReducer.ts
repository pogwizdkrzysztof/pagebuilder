import {ReducerCreator} from "../lib/redux/ReducerCreator";
import {ColumnModule} from "../stage/modules/ColumnModule";
import {ImageModule} from "../stage/modules/ImageModule";
import {SectionModule} from "../stage/modules/SectionModule";
import {TitledSectionModule} from "../stage/modules/TitledSectionModule";
import {TitleModule} from "../stage/modules/TitleModule";
import {ModulesState} from "./states/ModulesState";

export class ModulesReducer extends ReducerCreator<ModulesState> {

	public initialState = [
		// new MainModule(),
		new ImageModule(),
		new TitleModule(),
		new SectionModule(),
		new TitledSectionModule(),
		new ColumnModule(),
	];

	public spec = {};

}