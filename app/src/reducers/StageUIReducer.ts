import {combineReducers, Reducer} from "redux";
import {dragAndDrop, DragAndDropState} from "./DragAndDropReducer";
import {outlines, OutlinesState} from "./OutlinesReducer";
import {toolbars, ToolbarsState} from "./ToolbarReducer";

export interface StageUIState {
	readonly dragAndDrop:DragAndDropState,
	readonly toolbars:ToolbarsState,
	readonly outlines:OutlinesState,
}

export const stageUI:Reducer<StageUIState> = combineReducers<StageUIState>({
	dragAndDrop,
	toolbars,
	outlines: outlines,
});