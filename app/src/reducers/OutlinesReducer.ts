import remove from "lodash/remove";
import uniq from "lodash/uniq";
import {Reducer} from "redux";
import {OutlinesAction} from "../actions/OutlinesActions";
import {OutlinesActionsTypes} from "../actions/types/OutlinesActionsTypes";

export interface OutlinesState {
	readonly byId:{
		readonly [key:string]:ClientRect
	},
	readonly allIds:Array<string>;
}

const initialState:OutlinesState = {
	byId: {},
	allIds: [],
};

export const outlines:Reducer<OutlinesState, OutlinesAction> =
	(state = initialState, action) => {
		switch (action.type) {

			case OutlinesActionsTypes.ADD_MODULE_OUTLINE:
				return {
					byId: {
						...state.byId,
						[action.moduleId]: action.rect,
					},
					allIds: uniq([...state.allIds, action.moduleId]),
				};
			case OutlinesActionsTypes.REMOVE_MODULE_OUTLINE:
				let moduleId = action.moduleId;
				let _byId = {...state.byId};
				delete _byId[moduleId];
				return {
					byId: _byId,
					allIds: remove(state.allIds, moduleId),
				};
			case OutlinesActionsTypes.CLEAR_MODULES_OUTLINE:
				return initialState;
			default:
				return state;
		}
	};