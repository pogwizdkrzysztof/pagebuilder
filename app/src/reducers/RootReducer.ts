import {combineReducers, Reducer} from "redux";
import {AppAction} from "../actions/AppActions";
import {DebugAction} from "../actions/DebugActions";
import {DebugActionsTypes} from "../actions/types/DebugActionsTypes";
import {createReducer} from "../lib/redux/ReduxDecorators";
import {StorageUtil} from "../lib/utils/StorageUtil";
import {AppReducer} from "./AppReducer";
import {appUI, AppUIState} from "./AppUIReducer";
import {DebugReducer} from "./DebugReducer";
import {ModulesReducer} from "./ModulesReducer";
import {stage, StageState} from "./StageReducer";
import {stageUI, StageUIState} from "./StageUIReducer";
import {AppState} from "./states/AppState";
import {DebugState} from "./states/DebugState";
import {ModulesState} from "./states/ModulesState";

export interface RootState {
	readonly app:AppState,
	readonly appUI:AppUIState,
	readonly stage:StageState,
	readonly stageUI:StageUIState,
	readonly modules:ModulesState,
	readonly debug:DebugState
}


const GeneralReducer:Reducer<RootState> = combineReducers<RootState>({
	app: createReducer<AppState, AppAction>(new AppReducer()),
	appUI,
	stage,
	stageUI,
	modules: createReducer<ModulesState>(new ModulesReducer()),
	debug: createReducer<DebugState, DebugAction>(new DebugReducer()),
});

export const RootReducer = (state, action) => {
	if (action.type === DebugActionsTypes.LOAD_STATE) {
		state = StorageUtil.loadState();
	}

	return GeneralReducer(state, action);
};