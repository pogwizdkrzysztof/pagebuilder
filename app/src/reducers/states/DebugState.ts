export interface DebugState {
	readonly showIds:boolean,
	readonly showBorders:boolean
}