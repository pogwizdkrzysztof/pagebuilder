import {IModule} from "../../stage/modules/Module";

export interface ModulesState extends Array<IModule> {
	readonly [key:number]:IModule
}