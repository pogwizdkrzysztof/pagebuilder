import {AppStatus} from "../../const/AppStatus";

export interface AppState {
	readonly status:AppStatus
}