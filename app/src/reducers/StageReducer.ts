import keyBy from "lodash/keyBy";
import map from "lodash/map";
import mapValues from "lodash/mapValues";
import remove from "lodash/remove";
import uniq from "lodash/uniq";
import xor from "lodash/xor";
import {Reducer} from "redux";
import {DragAndDropAction} from "../actions/DragAndDropActions";
import {StageAction} from "../actions/StageActions";
import {DragAndDropActionsTypes} from "../actions/types/DragAndDropActionsTypes";
import {StageActionsTypes} from "../actions/types/StageActionsTypes";
import {UndoRedoActionsTypes} from "../actions/types/UndoRedoActionsTypes";
import {UndoRedoAction} from "../actions/UndoRedoActions";
import {MoveDirection} from "../const/MoveDirection";
import {ObjectUtil} from "../lib/utils/ObjectUtil";
import {stageModulesInitialState} from "../MockInitialState";
import {ModuleData} from "../stage/modules/data/ModuleData";
import {ModulesUtil} from "../stage/modules/utils/ModulesUtil";


export interface StageModulesState {
	readonly byId:{
		readonly [key:string]:ModuleData
	},
	readonly allIds:Array<string>;
}

export interface StageState {
	readonly selectedModuleId:string,
	readonly past:Array<StageModulesState>,
	readonly present:StageModulesState,
	readonly future:Array<StageModulesState>,
	readonly temporary:StageModulesState,
}

const initialState = {
	selectedModuleId: null,
	past: [],
	present: {
		allIds: [],
		byId: {},
	},
	future: [],
	temporary: {
		allIds: [],
		byId: {},
	},
};

const savePrevState = (state:StageState) => {
	return {
		...state,
		past: [...state.past, state.present],
		future: [],
	};
};

export const stage:Reducer<StageState, StageAction | UndoRedoAction | DragAndDropAction> = (state = stageModulesInitialState,
	action) => {

	let _byId = {};
	let _allIds:string[] = [];
	let _module = null;
	let _parentModule = null;
	let _moduleId = "";
	let _moduleIndex = -1;

	const {past, present, future, temporary} = state;

	switch (action.type) {
		case UndoRedoActionsTypes.UNDO:
			const previous = past[past.length - 1];
			const newPast = past.slice(0, past.length - 1);
			return {
				...state,
				past: newPast,
				present: previous,
				future: [present, ...future],
				temporary: initialState.temporary,
			};
		case UndoRedoActionsTypes.REDO:
			const next = future[0];
			const newFuture = future.slice(1);
			return {
				...state,
				past: [...past, present],
				present: next,
				future: newFuture,
				temporary: initialState.temporary,
			};

		case StageActionsTypes.ADD_STAGE_MODULE:
		{
			const module = action.modulesDatas[0];
			const mainModule = ObjectUtil.deepClone<ModuleData>(action.parentModule);
			ModulesUtil.addSubmodule(mainModule, module);

			_byId = {
				[mainModule.id]: mainModule,
				...mapValues(keyBy(action.modulesDatas, 'id')),
			};

			_allIds = [
				...map(action.modulesDatas, 'id'),
			];

			return {
				...savePrevState(state),
				present: {
					byId: {
						...state.present.byId,
						..._byId,
					},
					allIds: [
						...state.present.allIds,
						..._allIds,
					],
				},
			};
		}
		case StageActionsTypes.SAVE_STAGE_MODULE:
		{
			_byId = {
				...mapValues(keyBy(action.modulesDatas, 'id')),
			};

			_allIds = uniq([
				...state.present.allIds,
				...map(action.modulesDatas, 'id'),
			]);

			return {
				...savePrevState(state),
				temporary: initialState.temporary,
				present: {
					byId: {
						...state.present.byId,
						..._byId,
					},
					allIds: [
						..._allIds,
					],
				},
			};
		}
		case StageActionsTypes.CLONE_STAGE_MODULE:
		{ //@todo: improve whole block
			_module = ObjectUtil.deepClone(action.modulesDatas[0]);
			ModulesUtil.regenerateId(_module);
			let parentModule = state.present.byId[_module.parentId];

			let clonedModules = ModulesUtil.deepClone(_module, state.present.byId);
			let clonedParentModule = ObjectUtil.deepClone<ModuleData>(parentModule);

			let submoduleIndex = parentModule.submodulesIds.indexOf(action.modulesDatas[0].id) + 1;
			ModulesUtil.setParent(_module, clonedParentModule, submoduleIndex);
			clonedModules.push(clonedParentModule);

			let clonedModulesById = ModulesUtil.arrayToById(clonedModules);
			let clonedIds = ModulesUtil.getList(_module, clonedModulesById);

			return {
				...savePrevState(state),
				present: {
					byId: {
						...state.present.byId,
						...clonedModulesById,
					},
					allIds: [
						...state.present.allIds,
						...clonedIds,
					],
				}
			};
		}
		case StageActionsTypes.REMOVE_STAGE_MODULE:
		{

			_byId = {...state.present.byId};
			_allIds = [...state.present.allIds];

			var deletedModule = action.modulesDatas[0];
			_parentModule = ObjectUtil.deepClone(_byId[deletedModule.parentId]);
			var modulesToDelete = ModulesUtil.getList(deletedModule, _byId);

			remove(_parentModule.submodulesIds, (id) => id == deletedModule.id);
			_byId = {
				..._byId,
				[_parentModule.id]: _parentModule,
			};

			modulesToDelete.forEach(moduleId => {
				remove(_allIds, (id) => id == moduleId);
				delete _byId[moduleId];
			});

			return {
				...savePrevState(state),
				present: {
					byId: _byId,
					allIds: _allIds,
				},
			};
		}
		case StageActionsTypes.MOVE_STAGE_MODULE:

			_byId = ObjectUtil.clone(state.present.byId);
			_allIds = [...state.present.allIds];
			_module = action.modulesDatas[0];
			_parentModule = ObjectUtil.deepClone(_byId[_module.parentId]);
			_moduleId = _module.id;
			_moduleIndex = _parentModule.submodulesIds.indexOf(_moduleId);

			let isFirstAndMovedUp = _moduleIndex == 0 && action.moveDirection == MoveDirection.UP;
			let isLastAndMovedDown = _moduleIndex == _parentModule.submodulesIds.length - 1
									 && action.moveDirection == MoveDirection.DOWN;

			if (isFirstAndMovedUp || isLastAndMovedDown) {
				return {
					...savePrevState(state),
					present: {
						byId: _byId,
						allIds: _allIds,
					},
				};
			}

			let start = -1;
			let deleteCount = 2;
			let first = "";
			let second = "";

			switch (action.moveDirection) {
				case MoveDirection.UP:
					start = _moduleIndex - 1;
					first = _parentModule.submodulesIds[_moduleIndex];
					second = _parentModule.submodulesIds[_moduleIndex - 1];
					break;
				case MoveDirection.DOWN:
					start = _moduleIndex;
					first = _parentModule.submodulesIds[_moduleIndex + 1];
					second = _parentModule.submodulesIds[_moduleIndex];
					break;
			}
			_parentModule.submodulesIds.splice(start, deleteCount, first, second);

			_byId[_parentModule.id] = _parentModule;

			return {
				...savePrevState(state),
				present: {
					byId: _byId,
					allIds: _allIds,
				},
			};

		case DragAndDropActionsTypes.DROP_STAGE_MODULE:

			_byId = ObjectUtil.clone(state.present.byId);
			_byId = Object.assign(
				_byId,
				...action.modulesToUpdate,
			);

			_module = action.modulesDatas[0];
			_byId[_module.id] = _module;

			_allIds = [...state.present.allIds];
			if (_allIds.indexOf(action.modulesDatas[0].id) === -1)
				_allIds = [...state.present.allIds, action.modulesDatas[0].id];

			return {
				...savePrevState(state),
				present: {
					byId: _byId,
					allIds: _allIds,
				},
			};


		case StageActionsTypes.SELECT_STAGE_MODULE:
			return {
				...state,
				selectedModuleId: action.moduleDataId,
			};
		case StageActionsTypes.DESELECT_STAGE_MODULE:
			return {
				...state,
				selectedModuleId: null,
			};
		case StageActionsTypes.EDIT_STAGE_MODULE:
			const shouldDeselect = !xor(state.temporary.allIds, action.allIds).length;
			if (shouldDeselect) {
				return {
					...state,
					temporary: initialState.temporary,
				};
			}
			return {
				...state,
				temporary: {
					allIds: [...action.allIds],
					byId: {...action.byId},
				},
			};
		case StageActionsTypes.UPDATE_STAGE_MODULE:
			_byId = Object.assign(
				{},
				state.temporary.byId,
				{[action.modulesDatas[0].id]: action.modulesDatas[0]},
			);
			return {
				...state,
				temporary: {
					allIds: state.temporary.allIds,
					byId: _byId,
				},
			};
		case StageActionsTypes.ADD_CHILD_STAGE_MODULE:
			_allIds = [...state.temporary.allIds, action.modulesDatas[1].id];
			_byId = Object.assign(
				{},
				state.temporary.byId,
				{[action.modulesDatas[0].id]: action.modulesDatas[0]},
				{[action.modulesDatas[1].id]: action.modulesDatas[1]},
			);
			return {
				...state,
				temporary: {
					allIds: _allIds,
					byId: _byId,
				},
			};
		case StageActionsTypes.REMOVE_CHILD_STAGE_MODULE:
			_allIds = [action.modulesDatas[0].id, ...action.modulesDatas[0].submodulesIds];
			_byId = Object.assign(
				{},
				state.temporary.byId,
				{[action.modulesDatas[0].id]: action.modulesDatas[0]},
			);
			delete _byId[action.submoduleId];
			return {
				...state,
				temporary: {
					allIds: _allIds,
					byId: _byId,
				},
			};
		case StageActionsTypes.RESTORE_STAGE_MODULE:
			// case StageActionsTypes.SAVE_STAGE_MODULE:
			return {
				...state,
				temporary: initialState.temporary,
			};
		case StageActionsTypes.FORCE_UPDATE_STAGE_MODULE:
			_byId = Object.assign(
				{},
				state.present.byId,
				{[action.modulesDatas[0].id]: ObjectUtil.clone(action.modulesDatas[0])},
			);
			return {
				...state,
				present: {
					byId: _byId,
					allIds: state.present.allIds,
				},
			};

		default:
			return state;
	}
};