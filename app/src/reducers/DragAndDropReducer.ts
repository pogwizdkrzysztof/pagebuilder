import {Reducer} from "redux";
import {DragAndDropAction} from "../actions/DragAndDropActions";
import {DragAndDropActionsTypes} from "../actions/types/DragAndDropActionsTypes";
import {DropSide} from "../dnd/const/DropSide";
import {ObjectUtil} from "../lib/utils/ObjectUtil";

export interface DragAndDropState {
	readonly isDragging?:boolean,
	readonly dragModuleId?:string,
	readonly dropOverModuleId?:string,
	readonly dropModuleId?:string,
	readonly dropOverModuleRect?:ClientRect,
	readonly dropSide?:DropSide,
}

const initialState:DragAndDropState = {
	isDragging: false,
	dragModuleId: null,
	dropOverModuleId: null,
	dropModuleId: null,
	dropOverModuleRect: null,
	dropSide: null
};

export const dragAndDrop:Reducer<DragAndDropState, DragAndDropAction> = (state = initialState, action) => {
	switch (action.type) {
		case DragAndDropActionsTypes.BEGIN_DRAG:
			return {
				...initialState,
				isDragging: true,
			};
		case DragAndDropActionsTypes.END_DRAG:
			return initialState;
		case DragAndDropActionsTypes.DROP_STAGE_MODULE:
			return ObjectUtil.clone(initialState);
		case DragAndDropActionsTypes.UPDATE_DROP_INFO:
			return {
				...state,
				dropOverModuleId: action.dropOverModuleId,
				dropModuleId: action.dropModuleId,
				dropOverModuleRect: action.dropOverModuleRect,
				dropSide: action.dropSide,
				dragModuleId: action.dragModuleId,
			};
		default:
			return state;
	}
}