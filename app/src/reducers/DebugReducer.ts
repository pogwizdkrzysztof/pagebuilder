import {DebugAction} from "../actions/DebugActions";
import {DebugActionsTypes} from "../actions/types/DebugActionsTypes";
import {ReducerCreator} from "../lib/redux/ReducerCreator";
import {DebugState} from "./states/DebugState";

export class DebugReducer extends ReducerCreator<DebugState, DebugAction> {

	public initialState = {
		showIds: false,
		showBorders: false,
	};

	public spec = {
		[DebugActionsTypes.TOGGLE_IDS]: this.toggleIds,
		[DebugActionsTypes.TOGGLE_BORDERS]: this.toggleBorders,
	};

	public toggleIds(state:DebugState, action:DebugAction) {
		return {
			...state,
			showIds: !state.showIds,
		};
	}

	public toggleBorders(state:DebugState, action:DebugAction) {
		return {
			...state,
			showBorders: !state.showBorders,
		};
	}

}