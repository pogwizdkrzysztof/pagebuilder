import remove from "lodash/remove";
import uniq from "lodash/uniq";
import {Reducer} from "redux";
import {ToolbarsAction} from "../actions/ToolbarsActions";
import {ToolbarsActionsTypes} from "../actions/types/ToolbarsActionsTypes";
import {ToolbarData} from "../stageUI/ToolbarData";

export interface ToolbarsState {
	readonly byId:{
		readonly [key:string]:ToolbarData
	},
	readonly allIds:Array<string>;
}

const initialState:ToolbarsState = {
	byId: {},
	allIds: [],
};

export const toolbars:Reducer<ToolbarsState, ToolbarsAction> =
	(state = initialState, action) => {
		switch (action.type) {
			case ToolbarsActionsTypes.ADD_MODULE_TOOLBAR:
				let toolbarData = action.moduleToolbar;
				return {
					byId: {
						...state.byId,
						[toolbarData.moduleId]: toolbarData,
					},
					allIds: uniq([...state.allIds, toolbarData.moduleId]),
				};
			case ToolbarsActionsTypes.REMOVE_MODULE_TOOLBAR:
				let moduleId = action.moduleId;
				let _byId = {...state.byId};
				delete _byId[moduleId];
				return {
					byId: _byId,
					allIds: remove(state.allIds, moduleId),
				};
			case ToolbarsActionsTypes.CLEAR_MODULES_TOOLBARS:
				return initialState;
			default:
				return state;
		}
	};