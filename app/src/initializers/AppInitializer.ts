import {AppActions} from "../actions/AppActions";
import {AppStatus} from "../const/AppStatus";
import {EnvironmentModel} from "../lib/data/EnvironmentModel";
import {M} from "../models/M";
import {StoreModel} from "../models/StoreModel";


export class AppInitializer {

	public static init() {
		M.env = new EnvironmentModel();
		M.store = new StoreModel();

		AppActions.load(AppStatus.REQUEST);
	}

}