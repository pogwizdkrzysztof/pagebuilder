const utils = require('./utils');
const ForkTsCheckerNotifierWebpackPlugin = require('fork-ts-checker-notifier-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const config = require("./config");
const glob = require("glob");
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const webpackConfig = require('./webpack.config.js');

delete webpackConfig.entry;

module.exports = webpackMerge(webpackConfig, {
	mode: "none",
	cache: false,
	devtool: 'source-map',
	entry: {
		test: utils.path('app/tests/index.ts'),
	},
	node: {
		fs: 'empty',
	},
});